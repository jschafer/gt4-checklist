
                       .d8888b.
                      d88P  Y88b
                      888    888
                      888        888d888 8888b.  88888b.
                      888  88888 888P"      "88b 888 "88b
                      888    888 888    .d888888 888  888
                      Y88b  d88P 888    888  888 888  888
                       "Y8888P88 888    "Y888888 888  888


    88888888888               d8b                                        d8888
        888                   Y8P                                       d8P888
        888                                                            d8P 888
        888  888  888 888d888 888 .d8888b  88888b.d88b.   .d88b.      d8P  888
        888  888  888 888P"   888 88K      888 "888 "88b d88""88b    d88   888
        888  888  888 888     888 "Y8888b. 888  888  888 888  888    8888888888
        888  Y88b 888 888     888      X88 888  888  888 Y88..88P          888
        888   "Y88888 888     888  88888P' 888  888  888  "Y88P"           888

                  ____                     ______      _     __
                 / __ \____ _________     / ____/_  __(_)___/ /__
                / /_/ / __ `/ ___/ _ \   / / __/ / / / / __  / _ \
               / _, _/ /_/ / /__/  __/  / /_/ / /_/ / / /_/ /  __/
              /_/ |_|\__,_/\___/\___/   \____/\__,_/_/\__,_/\___/

                           __   ______              __    _      __
          ____ _____  ____/ /  / ____/___ ______   / /   (_)____/ /_
         / __ `/ __ \/ __  /  / /   / __ `/ ___/  / /   / / ___/ __/
        / /_/ / / / / /_/ /  / /___/ /_/ / /     / /___/ (__  ) /_
        \__,_/_/ /_/\__,_/   \____/\__,_/_/      \____/_/____/\__/

# Table of Contents

## Description

## 1 Overall Game Completion

## 2 License Center
- [ ] 2.1 National B
- [ ] 2.2 National A
- [ ] 2.3 International B
- [ ] 2.4 International A
- [ ] 2.5 Super

## 3 Driving Missions
- [ ] 3.1 (1-10)  The Pass
- [ ] 3.2 (11-20) 3 Lap Battle
- [ ] 3.3 (21-24) Slipstream Battle
- [ ] 3.4 (25-29) 1 Lap Magic
- [ ] 3.5 (30-34) 1 Lap Magic

## 4 Special Condition Events
- [ ] 4.1 Easy
- [ ] 4.2 Normal
- [ ] 4.3 Hard

## 5 Beginner Events
- [ ] 5.1 Sunday Cup
- [ ] 5.2 FF Challenge
- [ ] 5.3 FR Challenge
- [ ] 5.4 4WD Challenge
- [ ] 5.5 MR Challenge
- [ ] 5.6 Light-weight K-Car Cup
- [ ] 5.7 Spider & Roadster
- [ ] 5.8 Sport Truck Race

## 6 Professional Events
- [ ] 6.1 Clubman Cup
- [ ] 6.2 Tuning Car Grand Prix
- [ ] 6.3 Race of NA Sports
- [ ] 6.4 Race of Turbo Sports
- [ ] 6.5 Boxer Spirit
- [ ] 6.6 World Classic Car Series
- [ ] 6.7 World Compact Car Race
- [ ] 6.8 Supercar Festival
- [ ] 6.9 Gran Turismo World Championship

## 7 Extreme Events
- [ ] 7.1 Gran Turismo All Stars
- [ ] 7.2 Dream Car Championship
- [ ] 7.3 Polyphony Digital Cup
- [ ] 7.4 Like the Wind
- [ ] 7.5 Formula GT World Championship
- [ ] 7.6 World Circuit Tours
- [ ] 7.7 Premium Sports Lounge

## 8 Endurance Events
- [ ] 8.1 Grand Valley 300 km
- [ ] 8.2 Laguna Seca 200 miles
- [ ] 8.3 Roadster 4h Endurance
- [ ] 8.4 Tokyo R246 300 km
- [ ] 8.5 Super Speedway 150 miles
- [ ] 8.6 Nurburgring 24h Endurance
- [ ] 8.7 Nurburgring 4h Endurance
- [ ] 8.8 Suzuka 1000 km
- [ ] 8.9 Motegi 8h Endurance
- [ ] 8.10 Tsukuba 9h Endurance
- [ ] 8.11 Circuit de la Sarthe 24h I
- [ ] 8.12 Circuit de la Sarthe 24h II
- [ ] 8.13 Fuji 1000 km
- [ ] 8.14 Infineon World Sports
- [ ] 8.15 El Capitan 200 miles
- [ ] 8.16 New York 200 miles

## 9 American Events
- [ ] 9.1 All American Championship
- [ ] 9.2 Stars and Stripes
- [ ] 9.3 Hot Rod Competition
- [ ] 9.4 Muscle Car Competition

## 10 European Events
- [ ] 10.1 Pan Euro Championship
- [ ] 10.2 British GT Series
- [ ] 10.3 British Lightweight Series
- [ ] 10.4 Deutsche Touring Car Meisterschaft
- [ ] 10.5 La Festa Italiano
- [ ] 10.6 Tous France Championnat
- [ ] 10.7 Europe Classic Car League
- [ ] 10.8 Euro Hot Hatch League
- [ ] 10.9 1000 miles !
- [ ] 10.10 Schwarzwald Liga A
- [ ] 10.11 Schwarzwald Liga B

## 11 Japanese Events
- [ ] 11.1 Japan Championship
- [ ] 11.2 All Japan GT Championship
- [ ] 11.3 Japanese 70's Classics
- [ ] 11.4 Japanese 80's Festival
- [ ] 11.5 Japanese 90's Challenge
- [ ] 11.6 Japanese Compact Cup

## 12 One-make Races: France
- [ ] 12.1 Alpine: Renault Alpine Cup
- [ ] 12.2 Citroen: 2HP - 2CV Classics
- [ ] 12.3 Peugeot: 206 Cup
- [ ] 12.4 Renault: Clio Trophy
- [ ] 12.5 Renault: Megane Cup

## 13 One-make Races: Germany
- [ ] 13.1 Audi: Tourist Trophy
- [ ] 13.2 Audi: A3 Cup
- [ ] 13.3 BMW: 1 Series Trophy
- [ ] 13.4 BMW: Club "M"
- [ ] 13.5 Mercedes Benz: Legends of the Silver Arrow
- [ ] 13.6 Mercedes Benz: SL Challenge
- [ ] 13.7 Opel: Speedster Trophy
- [ ] 13.8 Volkswagen: Beetle Cup
- [ ] 13.9 Volkswagen: Lupo Cup
- [ ] 13.10 Volkswagen: GTi Cup

## 14 One-make Races: Italy
- [ ] 14.1 Alfa Romeo: GTA Cup

## 15 One-make Races: Japan
- [ ] 15.1 Daihatsu: Copen Race
- [ ] 15.2 Daihatsu: Midget II Race
- [ ] 15.3 Honda: Type R Meeting
- [ ] 15.4 Honda: Civic Race
- [ ] 15.5 Isuzu: Isuzu Sports Classics
- [ ] 15.6 Mazda: Club "RE"
- [ ] 15.7 Mazda: NR-A Roadster Cup
- [ ] 15.8 Mazda: NR-A RX-8 Cup
- [ ] 15.9 Mitsubishi: Evolution Meeting
- [ ] 15.10 Mitsubishi: Mirage Cup
- [ ] 15.11 Nissan: Race of the Red "R" Emblem
- [ ] 15.12 Nissan: March Brothers
- [ ] 15.13 Nissan: Silvia Sisters
- [ ] 15.14 Nissan: Club "Z"
- [ ] 15.15 Subaru: Subaru 360 Race
- [ ] 15.16 Subaru: Stars of Pleiades
- [ ] 15.17 Suzuki: Suzuki K-Car Cup
- [ ] 15.18 Suzuki: Suzuki Concepts
- [ ] 15.19 Toyota: Altezza Race
- [ ] 15.20 Toyota: Vitz Race

## 16 One-make Races: Korea
- [ ] 16.1 Hyundai: Hyundai Sports Festival

## 17 One-make Races: UK
- [ ] 17.1 Aston Martin: Aston Martin Carnival
- [ ] 17.2 Lotus: Elise Trophy
- [ ] 17.3 Lotus: Lotus Classics
- [ ] 17.4 Mini: Mini Mini Sports Meeting
- [ ] 17.5 MG: MG Festival
- [ ] 17.6 Triumph: Spitfire Cup
- [ ] 17.7 TVR: Black Pool Racers

## 18 One-make Races: USA
- [ ] 18.1 Chevrolet: Vette! Vette! Vette!
- [ ] 18.2 Chevrolet: Camaro Meeting
- [ ] 18.3 Chrysler: Crossfire Trophy
- [ ] 18.4 Saleen: Saleen S7 Club
- [ ] 18.5 Shelby: Shelby Cobra Cup

## 19 Car List

-------------------------------------------------------------------------------
## Description
-------------------------------------------------------------------------------

The guide also allows you to keep track of your best A-Spec points for each 
race.  For each track at every event there is an A-Spec points bar.  This bar 
is provided after the completion checkbox and before the duration/track.  The
lines listing the tracks are formatted like this:

  - [ ] `[ | | | | | | | ]` \<DURATION> - \<TRACK>

The A-Spec points bar is the part that looks like `[ | | | | | | | ]`.  It is
divided into eight sections where each represents 25 points.  To use the bar,
check off the appropriate number of sections for each 25 points earned for the
race.  For example:

`[ | | | | | | | ]` = 0-24 points earned  
`[X| | | | | | | ]` = 25-49 points earned  
`[X|X| | | | | | ]` = 50-74 points earned  
`[X|X|X| | | | | ]` = 75-99 points earned  
`[X|X|X|X| | | | ]` = 100-124 points earned  
`[X|X|X|X|X| | | ]` = 125-149 points earned  
`[X|X|X|X|X|X| | ]` = 150-174 points earned  
`[X|X|X|X|X|X|X| ]` = 175-199 points earned  
`[X|X|X|X|X|X|X|X]` = 200 points earned

It allows you to see how many A-Spec points you earned (to within 25 points)
without having to write down the exact number and having to cross out or erase
when it gets updated.

-------------------------------------------------------------------------------
## 1 Overall Game Completion
-------------------------------------------------------------------------------

In order to get 100% completion, all driving missions, races, and
championships must be won (basically everything in sections 3 through 18).
Note that if you won a championship but failed to win one or more races in the
event, you must rerun and win those individual races to get 100%.

Obtaining licenses does not increase your percent completed and therefore,
getting all gold licenses is not required.  However, licenses are required
to enter all the driving missions and most of the races/championships in
A-Spec.

- [ ] 25%  2003 Audi Nuvolari Quattro
- [ ] 50%  1988 Jaguar XJR-9 Race Car
- [ ] 100% 2004 Polyphony Digital Formula Gran Turismo (black)

-------------------------------------------------------------------------------
## 2 License Center
-------------------------------------------------------------------------------

### 2.1 National B

- [ ] (Bronze) 2002 Volkswagen Lupo 1.4
- [ ] (Silver) 2003 Mazda Kusabi
- [ ] (Gold)   1963 Honda S500

B-1 Acceleration and Braking: 1
  - [ ] Bronze
  - [ ] Silver
  - [ ] Gold

B-2 Acceleration and Braking: 2
  - [ ] Bronze
  - [ ] Silver
  - [ ] Gold

B-3 Basics of Cornering: 1
  - [ ] Bronze
  - [ ] Silver
  - [ ] Gold

B-4 Basics of Cornering: 2
  - [ ] Bronze
  - [ ] Silver
  - [ ] Gold

B-5 1 Lap Guide Run (Tsukuba)
  - [ ] Bronze
  - [ ] Silver
  - [ ] Gold

B-6 Acceleration and Braking: 3
  - [ ] Bronze
  - [ ] Silver
  - [ ] Gold

B-7 Acceleration and Braking: 4
  - [ ] Bronze
  - [ ] Silver
  - [ ] Gold

B-8 Basics of Cornering: 3
  - [ ] Bronze
  - [ ] Silver
  - [ ] Gold

B Coffee Break: Maneuver around the cones
  - [ ] Bronze
  - [ ] Silver
  - [ ] Gold

B-9 Basics of Cornering: 4
  - [ ] Bronze
  - [ ] Silver
  - [ ] Gold

B-10 1 Lap Guide Run (Laguna Seca)
  - [ ] Bronze
  - [ ] Silver
  - [ ] Gold

B-11 Basics of Dirt Driving: 1
  - [ ] Bronze
  - [ ] Silver
  - [ ] Gold

B-12 Basics of Dirt Driving: 2
  - [ ] Bronze
  - [ ] Silver
  - [ ] Gold

B-13 Basics of Cornering: 5
  - [ ] Bronze
  - [ ] Silver
  - [ ] Gold

B-14 Basics of Cornering: 6
  - [ ] Bronze
  - [ ] Silver
  - [ ] Gold

B-15 1 Lap Guide Run (Infineon Raceway-Sports Car Course)
  - [ ] Bronze
  - [ ] Silver
  - [ ] Gold

B-16 Graduation Test - Tsukuba Circuit
  - [ ] Bronze
  - [ ] Silver
  - [ ] Gold

### 2.2 National A

- [ ] (Bronze) 2002 Pontiac Sunfire GXP Concept
- [ ] (Silver) 2002 Acura DN-X
- [ ] (Gold)   2001 Nissan Gran Turismo Skyline GT-R

A-1 Complex Corners: Beginner 1
  - [ ] Bronze
  - [ ] Silver
  - [ ] Gold

A-2 Complex Corners: Beginner 2
  - [ ] Bronze
  - [ ] Silver
  - [ ] Gold

A-3 Tackling High Speed Corners: 1
  - [ ] Bronze
  - [ ] Silver
  - [ ] Gold

A-4 Tackling High Speed Corners: 2
  - [ ] Bronze
  - [ ] Silver
  - [ ] Gold

A-5 1 Lap Guide Run (Grand Valley East)
  - [ ] Bronze
  - [ ] Silver
  - [ ] Gold

A-6 Complex Corners: Intermediate 1
  - [ ] Bronze
  - [ ] Silver
  - [ ] Gold

A-7 Complex Corners: Intermediate 2
  - [ ] Bronze
  - [ ] Silver
  - [ ] Gold

A-8 Applied Cornering: 1
  - [ ] Bronze
  - [ ] Silver
  - [ ] Gold

A Coffee Break: Knock over the cones laid out in a spiral
  - [ ] Bronze
  - [ ] Silver
  - [ ] Gold

A-9 Applied Cornering: 2
  - [ ] Bronze
  - [ ] Silver
  - [ ] Gold

A-10 1 Lap Guide Run (Suzuka Circuit)
  - [ ] Bronze
  - [ ] Silver
  - [ ] Gold

A-11 Applied Dirt Driving: 1
  - [ ] Bronze
  - [ ] Silver
  - [ ] Gold

A-12 Applied Dirt Driving: 2
  - [ ] Bronze
  - [ ] Silver
  - [ ] Gold

A-13 Tackling Blind Corners: 1
  - [ ] Bronze
  - [ ] Silver
  - [ ] Gold

A-14 Tackling Blind Corners: 2
  - [ ] Bronze
  - [ ] Silver
  - [ ] Gold

A-15 1 Lap Guide Run (Trial Mountain)
  - [ ] Bronze
  - [ ] Silver
  - [ ] Gold

A-16 Graduation Test - Nurburgring Nordschleife
  - [ ] Bronze
  - [ ] Silver
  - [ ] Gold

### 2.3 International B

- [ ] (Bronze) 2022 Nike One
- [ ] (Silver) 2001 Mazda RX-8 Concept (Type-II)
- [ ] (Gold)   1974 Jensen Healey Interceptor MkIII

IB-1 Braking During Turns: 1
  - [ ] Bronze
  - [ ] Silver
  - [ ] Gold

IB-2 Braking During Turns: 2
  - [ ] Bronze
  - [ ] Silver
  - [ ] Gold

IB-3 Advanced Level Complex Corners: 1
  - [ ] Bronze
  - [ ] Silver
  - [ ] Gold

IB-4 Advanced Level Complex Corners: 2
  - [ ] Bronze
  - [ ] Silver
  - [ ] Gold

IB-5 1 Lap Guide Run (Costa di Amalfi)
  - [ ] Bronze
  - [ ] Silver
  - [ ] Gold

IB-6 Cone Slalom 1
  - [ ] Bronze
  - [ ] Silver
  - [ ] Gold

IB-7 Cone Slalom 2
  - [ ] Bronze
  - [ ] Silver
  - [ ] Gold

IB-8 City Street Challenge: 1
  - [ ] Bronze
  - [ ] Silver
  - [ ] Gold

IB Coffee Break: Knock over the cones laid out in a spiral 2
  - [ ] Bronze
  - [ ] Silver
  - [ ] Gold

IB-9 City Street Challenge: 2
  - [ ] Bronze
  - [ ] Silver
  - [ ] Gold

IB-10 1 Lap Guide Run (Hong Kong)
  - [ ] Bronze
  - [ ] Silver
  - [ ] Gold

IB-11 Competition Dirt Racing: 1
  - [ ] Bronze
  - [ ] Silver
  - [ ] Gold

IB-12 Driving on Snow: 1
  - [ ] Bronze
  - [ ] Silver
  - [ ] Gold

IB-13 Tackling Undulating S-bends
  - [ ] Bronze
  - [ ] Silver
  - [ ] Gold

IB-14 Tackling Corner Sequences over Undulated Surfaces
  - [ ] Bronze
  - [ ] Silver
  - [ ] Gold

IB-15 1 Lap Guide Run (Citta di Aria)
  - [ ] Bronze
  - [ ] Silver
  - [ ] Gold

IB-16 Graduation Test - Citta di Aria
  - [ ] Bronze
  - [ ] Silver
  - [ ] Gold

### 2.4 International A

- [ ] (Bronze) 1994 Nismo 270R
- [ ] (Silver) 2001 Nissan GT-R Concept (Tokyo Show)
- [ ] (Gold)   1978 Dodge Dome Zero

IA-1 Tackling Complex Corner Sequences: 1
  - [ ] Bronze
  - [ ] Silver
  - [ ] Gold

IA-2 Tackling Complex Corner Sequences: 2
  - [ ] Bronze
  - [ ] Silver
  - [ ] Gold

IA-3 Guide to Chicanes: 1
  - [ ] Bronze
  - [ ] Silver
  - [ ] Gold

IA-4 Guide to Chicanes: 2
  - [ ] Bronze
  - [ ] Silver
  - [ ] Gold

IA-5 1 Lap Guide Run (El Capitan)
  - [ ] Bronze
  - [ ] Silver
  - [ ] Gold

IA-6 Tackling Complex Corner Sequences: 3
  - [ ] Bronze
  - [ ] Silver
  - [ ] Gold

IA-7 Tackling Complex Corner Sequences: 4
  - [ ] Bronze
  - [ ] Silver
  - [ ] Gold

IA-8 City Street Challenge: 3
  - [ ] Bronze
  - [ ] Silver
  - [ ] Gold

IA Coffee Break: Cones - Gymkhana
  - [ ] Bronze
  - [ ] Silver
  - [ ] Gold

IA-9 City Street Challenge: 4
  - [ ] Bronze
  - [ ] Silver
  - [ ] Gold

IA-10 1 Lap Guide Run (Fuji Speedway 2005)
  - [ ] Bronze
  - [ ] Silver
  - [ ] Gold

IA-11 Competition Dirt Racing: 2
  - [ ] Bronze
  - [ ] Silver
  - [ ] Gold

IA-12 Driving on Snow: 2
  - [ ] Bronze
  - [ ] Silver
  - [ ] Gold

IA-13 Tackling Complex Corner Sequences: 5
  - [ ] Bronze
  - [ ] Silver
  - [ ] Gold

IA-14 Tackling Complex Corner Sequences: 6
  - [ ] Bronze
  - [ ] Silver
  - [ ] Gold

IA-15 1 Lap Guide Run (Nurburgring North Course)
  - [ ] Bronze
  - [ ] Silver
  - [ ] Gold

IA-16 Graduation Test - Circuit de la Sarthe II
  - [ ] Bronze
  - [ ] Silver
  - [ ] Gold

### 2.5 Super

- [ ] (Bronze) 1967 Mercury Cougar XR-7
- [ ] (Silver) 2002 Pontiac Solstice Coupe Concept
- [ ] (Gold)   1915 Ford Model T Tourer

S-1 1 Lap Time Trial (Twin Ring Motegi Road Course)
  - [ ] Bronze
  - [ ] Silver
  - [ ] Gold

S-2 1 Lap Time Trial (Citta di Aria)
  - [ ] Bronze
  - [ ] Silver
  - [ ] Gold

S-3 1 Lap Time Trial (Special Stage Route 5)
  - [ ] Bronze
  - [ ] Silver
  - [ ] Gold

S-4 1 Lap Time Trial (Costa di Amalfi)
  - [ ] Bronze
  - [ ] Silver
  - [ ] Gold

S-5 1 Lap Time Trial (Seattle)
  - [ ] Bronze
  - [ ] Silver
  - [ ] Gold

S-6 1 Lap Time Trial (Ice Arena)
  - [ ] Bronze
  - [ ] Silver
  - [ ] Gold

S-7 1 Lap Time Trial (Trial Mountain)
  - [ ] Bronze
  - [ ] Silver
  - [ ] Gold

S-8 1 Lap Time Trial (Tokyo Route 246)
  - [ ] Bronze
  - [ ] Silver
  - [ ] Gold

S Coffee Break: Cone Maze Challenge
  - [ ] Bronze
  - [ ] Silver
  - [ ] Gold

S-9 1 Lap Time Trial (El Capitan)
  - [ ] Bronze
  - [ ] Silver
  - [ ] Gold

S-10 1 Lap Time Trial (Grand Canyon)
  - [ ] Bronze
  - [ ] Silver
  - [ ] Gold

S-11 1 Lap Time Trial (Opera Paris)
  - [ ] Bronze
  - [ ] Silver
  - [ ] Gold

S-12 1 Lap Time Trial (Suzuka Circuit)
  - [ ] Bronze
  - [ ] Silver
  - [ ] Gold

S-13 1 Lap Time Trial (Infineon Raceway-Sports Car Course)
  - [ ] Bronze
  - [ ] Silver
  - [ ] Gold

S-14 1 Lap Time Trial (Chamonix)
  - [ ] Bronze
  - [ ] Silver
  - [ ] Gold

S-15 1 Lap Time Trial (Circuit de la Sarthe)
  - [ ] Bronze
  - [ ] Silver
  - [ ] Gold

S-16 1 Lap Time Trial (Nurburgring North Course)
  - [ ] Bronze
  - [ ] Silver
  - [ ] Gold

-------------------------------------------------------------------------------
## 3 Driving Missions
-------------------------------------------------------------------------------

### 3.1 (1-10) The Pass

- [ ] 2004 DMC DeLorean S2

  - [ ] DM-1  - IB - Deep Forest - Deep Forest: 1st Corner
  - [ ] DM-2  - IB - Grand Valley - Grand Valley: 1st Corner
  - [ ] DM-3  - IB - Cote d' Azur - Cote d' Azur
  - [ ] DM-4  - IB - Seattle - Seattle: 1st Corner
  - [ ] DM-5  - IB - High-speed Ring - High-speed Ring: Corner 2-3
  - [ ] DM-6  - IB - Tsukuba - Tsukuba Circuit: Dunlop Bridge
  - [ ] DM-7  - IB - Midfield Raceway - Midfield Raceway: Final hairpin
  - [ ] DM-8  - IB - Trial Mountain - Trial Mountain: 1st Tunnel
  - [ ] DM-9  - IB - Motegi Oval - Twin Ring Motegi Road Course
  - [ ] DM-10 - IB - Sarthe I - Sarthe '04: Mulsanne Straight

### 3.2 (11-20) 3 Lap Battle

- [ ] 2003 Jay Leno Tank Car

  - [ ] DM-11 - IA - New York - New York Reverse
  - [ ] DM-12 - IA - Laguna Seca - Laguna Seca Raceway
  - [ ] DM-13 - IA - Opera Paris - Opera Paris
  - [ ] DM-14 - IA - Seattle - Seattle Circuit
  - [ ] DM-15 - IA - Cote d' Azur - Cote d' Azur
  - [ ] DM-16 - IA - Suzuka - Suzuka Circuit
  - [ ] DM-17 - IA - Infineon - Infineon Raceway (Sports Car Course)
  - [ ] DM-18 - IA - Sarthe II - Circuit de la Sarthe 2
  - [ ] DM-19 - IA - Suzuka East - Suzuka Circuit East
  - [ ] DM-20 - IA - Tsukuba - Tsukuba Circuit

### 3.3 (21-24) Slipstream Battle

- [ ] 2001 Pagani Zonda Race Car

  - [ ] DM-21 - IA - Nissan Cube - Test Course
  - [ ] DM-22 - IA - Honda Odyssey - Test Course
  - [ ] DM-23 - IA - Nissan Skyline GT-R - Test Course
  - [ ] DM-24 - IA - Amuse S2000 GT1 - Test Course

### 3.4 (25-29) 1 Lap Magic

- [ ] 1970 Toyota Toyota 7 Race Car

  - [ ] DM-25 - IA - Z Showdown! - Fuji Speedway 80's
  - [ ] DM-26 - IA - Honda Showdown! - Suzuka Circuit
  - [ ] DM-27 - IA - Mazda Showdown! - Laguna Seca Raceway
  - [ ] DM-28 - IA - Celica Showdown! - Fuji Speedway 2005
  - [ ] DM-29 - IA - Chevrolet Showdown! - Infineon Raceway (Sports Car Course)

### 3.5 (30-34) 1 Lap Magic

- [ ] 1989 Nissan R89C Race Car

  - [ ] DM-30 - IA - Subaru Showdown! - Tsukuba Circuit
  - [ ] DM-31 - IA - Lotus Showdown! - High Speed Ring Reverse
  - [ ] DM-32 - IA - Ford Showdown! - Seattle Circuit Reverse
  - [ ] DM-33 - IA - GM Showdown! - Infineon Raceway (Sports Car Course)
  - [ ] DM-34 - IA - Mercedes Showdown! - Nurburgring Nordschleife

-------------------------------------------------------------------------------
## 4 Special Condition Events
-------------------------------------------------------------------------------

### 4.1 Easy

|||
|---|---|
|Game Mode|Single Race|
|Tires|(see below)|
|Cars Permitted|All cars permitted|
|Required License|A-Spec: National A License|
|1st|5,000 cr|

### 4.1.1 Umbria Rally (Tires: No restrictions)

- [ ] 2002 Cadillac Cien

  - [ ] `[ | | | | | | | ]` 2 laps - Citta di Aria
  - [ ] `[ | | | | | | | ]` 2 laps - Citta di Aria Reverse

### 4.1.2 Capri Rally (Tires: No restrictions)

- [ ] 2002 Toyota RSC Rally Raid Car

  - [ ] `[ | | | | | | | ]` 2 laps - Costa di Amalfi
  - [ ] `[ | | | | | | | ]` 2 laps - Costa di Amalfi Reverse

### 4.1.3 Grand Canyon Rally (Tires: Dirt tires required)

- [ ] 1984 Ford RS200

  - [ ] `[ | | | | | | | ]` 2 laps - Grand Canyon
  - [ ] `[ | | | | | | | ]` 2 laps - Grand Canyon Reverse

### 4.1.4 Whistler Ice Race (Tires: Snow tires required)

- [ ] 2001 Toyota RSC

  - [ ] `[ | | | | | | | ]` 2 laps - Ice Arena
  - [ ] `[ | | | | | | | ]` 2 laps - Ice Arena Reverse

### 4.1.5 Chamonix Rally (Tires: Snow tires required)

- [ ] 2002 Infiniti FX45 Concept

  - [ ] `[ | | | | | | | ]` 2 laps - Chamonix
  - [ ] `[ | | | | | | | ]` 2 laps - Chamonix Reverse

### 4.1.6 George V Rally (Tires: No restrictions)

- [ ] 1973 Alpine A310 1600VE

  - [ ] `[ | | | | | | | ]` 2 laps - George V Paris
  - [ ] `[ | | | | | | | ]` 2 laps - George V Paris Reverse

### 4.1.7 Swiss Alps Rally (Tires: Dirt tires required)

- [ ] 2001 Mitsubishi CZ-3 Tarmac

  - [ ] `[ | | | | | | | ]` 2 laps - Swiss Alps
  - [ ] `[ | | | | | | | ]` 2 laps - Swiss Alps Reverse

### 4.1.8 Tour of Tahiti (Tires: Dirt tires required)

- [ ] 1980 Renault 5 Turbo

  - [ ] `[ | | | | | | | ]` 2 laps - Tahiti Maze
  - [ ] `[ | | | | | | | ]` 2 laps - Tahiti Maze Reverse

### 4.1.9 Tsukuba Wet Race (Tires: No restrictions)

- [ ] 2001 Mazda RX-8 Concept (Type-I)

  - [ ] `[ | | | | | | | ]` 2 laps - Tsukuba Circuit (Wet)

### 4.1.10 Yosemite Rally I (Tires: Dirt tires required)

- [ ] 2004 Land Rover Range Stormer Concept

  - [ ] `[ | | | | | | | ]` 2 laps - Cathedral Rocks Trial I
  - [ ] `[ | | | | | | | ]` 2 laps - Cathedral Rocks Trial I Reverse

### 4.1.11 Yosemite Rally II (Tires: Dirt tires required)

- [ ] 2001 Hyundai HCD6

  - [ ] `[ | | | | | | | ]` 2 laps - Cathedral Rocks Trial II
  - [ ] `[ | | | | | | | ]` 2 laps - Cathedral Rocks Trial II Reverse

### 4.2 Normal

|||
|---|---|
|Game Mode|Single Race|
|Tires|(see below)|
|Cars Permitted|All cars permitted|
|Required License|A-Spec: National B License|
|1st|10,000 cr|

### 4.2.1 Umbria Rally (Tires: No restrictions)

- [ ] 1992 Lancia Delta HF Integrale Rally Car

  - [ ] `[ | | | | | | | ]` 3 laps - Citta di Aria
  - [ ] `[ | | | | | | | ]` 3 laps - Citta di Aria Reverse

### 4.2.2 Capri Rally (Tires: No restrictions)

- [ ] 2001 Subaru Impreza Rally Car Prototype

  - [ ] `[ | | | | | | | ]` 3 laps - Costa di Amalfi
  - [ ] `[ | | | | | | | ]` 3 laps - Costa di Amalfi Reverse

### 4.2.3 Grand Canyon Rally (Tires: Dirt tires required)

- [ ] 2002 Mitsubishi CZ-3 Tarmac Rally Car

  - [ ] `[ | | | | | | | ]` 3 laps - Grand Canyon
  - [ ] `[ | | | | | | | ]` 3 laps - Grand Canyon Reverse

### 4.2.4 Whistler Ice Race (Tires: Snow tires required)

- [ ] 1997 Mitsubishi Lancer Evolution IV Rally Car

  - [ ] `[ | | | | | | | ]` 3 laps - Ice Arena
  - [ ] `[ | | | | | | | ]` 3 laps - Ice Arena Reverse

### 4.2.5 Chamonix Rally (Tires: Snow tires required)

- [ ] 2001 Subaru Impreza Rally Car

  - [ ] `[ | | | | | | | ]` 3 laps - Chamonix
  - [ ] `[ | | | | | | | ]` 3 laps - Chamonix Reverse

### 4.2.6 George V Rally (Tires: No restrictions)

- [ ] 1985 Renault 5 Maxi Turbo Rally Car

  - [ ] `[ | | | | | | | ]` 3 laps - George V Paris
  - [ ] `[ | | | | | | | ]` 3 laps - George V Paris Reverse

### 4.2.7 Swiss Alps Rally (Tires: Dirt tires required)

- [ ] 1995 Toyota Celica GT-Four Rally Car (ST205)

  - [ ] `[ | | | | | | | ]` 3 laps - Swiss Alps
  - [ ] `[ | | | | | | | ]` 3 laps - Swiss Alps Reverse

### 4.2.8 Tour of Tahiti (Tires: Dirt tires required)

- [ ] 1998 Ford Escort Rally Car

  - [ ] `[ | | | | | | | ]` 3 laps - Tahiti Maze
  - [ ] `[ | | | | | | | ]` 3 laps - Tahiti Maze Reverse

### 4.2.9 Tsukuba Wet Race (Tires: No restrictions)

- [ ] 2002 Mazda Mazda6 Touring Car

  - [ ] `[ | | | | | | | ]` 3 laps - Tsukuba Circuit (Wet)

### 4.2.10 Yosemite Rally I (Tires: Dirt tires required)

- [ ] 1999 Subaru Impreza Rally Car

  - [ ] `[ | | | | | | | ]` 3 laps - Cathedral Rocks Trial I
  - [ ] `[ | | | | | | | ]` 3 laps - Cathedral Rocks Trial I Reverse

### 4.2.11 Yosemite Rally II (Tires: Dirt tires required)

- [ ] 1995 Toyota Celica GT-Four Rally Car (ST185)

  - [ ] `[ | | | | | | | ]` 3 laps - Cathedral Rocks Trial II
  - [ ] `[ | | | | | | | ]` 3 laps - Cathedral Rocks Trial II Reverse

### 4.3 Hard

|||
|---|---|
|Game Mode|Single Race|
|Tires|(see below)|
|Cars Permitted|All cars permitted|
|Required License|A-Spec: International A License|
|1st|20,000 cr|

### 4.3.1 Umbria Rally (Tires: No restrictions)

- [ ] 1985 Lancia Delta S4 Rally Car

  - [ ] `[ | | | | | | | ]` 5 laps - Citta di Aria
  - [ ] `[ | | | | | | | ]` 5 laps - Citta di Aria Reverse

### 4.3.2 Capri Rally (Tires: No restrictions)

- [ ] 1985 Ford RS200 Rally Car

  - [ ] `[ | | | | | | | ]` 5 laps - Costa di Amalfi
  - [ ] `[ | | | | | | | ]` 5 laps - Costa di Amalfi Reverse

### 4.3.3 Grand Canyon Rally (Tires: Dirt tires required)

- [ ] 1984 Mitsubishi Starion 4WD Rally Car

  - [ ] `[ | | | | | | | ]` 5 laps - Grand Canyon
  - [ ] `[ | | | | | | | ]` 5 laps - Grand Canyon Reverse

### 4.3.4 Whistler Ice Race (Tires: Snow tires required)

- [ ] 1969 Nissan Bluebird Rally Car (510)

  - [ ] `[ | | | | | | | ]` 5 laps - Ice Arena
  - [ ] `[ | | | | | | | ]` 5 laps - Ice Arena Reverse

### 4.3.5 Chamonix Rally (Tires: Snow tires required)

- [ ] 1977 Lancia Stratos Rally Car

  - [ ] `[ | | | | | | | ]` 5 laps - Chamonix
  - [ ] `[ | | | | | | | ]` 5 laps - Chamonix Reverse

### 4.3.6 George V Rally (Tires: No restrictions)

- [ ] 1986 Peugeot 205 Turbo 16 Evolution 2 Rally Car

  - [ ] `[ | | | | | | | ]` 5 laps - George V Paris
  - [ ] `[ | | | | | | | ]` 5 laps - George V Paris Reverse

### 4.3.7 Swiss Alps Rally (Tires: Dirt tires required)

- [ ] 1985 Nissan 240RS Rally Car

  - [ ] `[ | | | | | | | ]` 5 laps - Swiss Alps
  - [ ] `[ | | | | | | | ]` 5 laps - Swiss Alps Reverse

### 4.3.8 Tour of Tahiti (Tires: Dirt tires required)

- [ ] 1985 Mitsubishi Pajero Rally Raid Car

  - [ ] `[ | | | | | | | ]` 5 laps - Tahiti Maze
  - [ ] `[ | | | | | | | ]` 5 laps - Tahiti Maze Reverse

### 4.3.9 Tsukuba Wet Race (Tires: No restrictions)

- [ ] 2002 Ford Ford GT Concept

  - [ ] `[ | | | | | | | ]` 5 laps - Tsukuba Circuit (Wet)

### 4.3.10 Yosemite Rally I (Tires: Dirt tires required)

- [ ] 1998 Suzuki Escudo Dirt Trial Car

  - [ ] `[ | | | | | | | ]` 5 laps - Cathedral Rocks Trial I
  - [ ] `[ | | | | | | | ]` 5 laps - Cathedral Rocks Trial I Reverse

### 4.3.11 Yosemite Rally II (Tires: Dirt tires required)

- [ ] 2003 Mitsubishi Pajero Evolution Rally Raid Car

  - [ ] `[ | | | | | | | ]` 5 laps - Cathedral Rocks Trial II
  - [ ] `[ | | | | | | | ]` 5 laps - Cathedral Rocks Trial II Reverse

-------------------------------------------------------------------------------
## 5 Beginner Events
-------------------------------------------------------------------------------

### 5.1 Sunday Cup

|||
|---|---|
|Game Mode|Single Race|
|Tires|Standard or Sports Tires required|
|Cars Permitted|All cars permitted|
|Required License|None|

|||||
|---|---|---|---|
|1st|600 cr|4th|300 cr|
|2nd|500 cr|5th|200 cr|
|3rd|400 cr|6th|100 cr|

- [ ] 1979 Autobianchi A112 Abarth

  - [ ] `[ | | | | | | | ]` 2 laps - Autumn Ring Mini Reverse
  - [ ] `[ | | | | | | | ]` 4 laps - Beginner Course
  - [ ] `[ | | | | | | | ]` 2 laps - High Speed Ring
  - [ ] `[ | | | | | | | ]` 2 laps - Clubman Stage Route 5 Reverse
  - [ ] `[ | | | | | | | ]` 3 laps - Twin Ring Motegi (West Short Course)

### 5.2 FF Challenge

|||
|---|---|
|Game Mode|Single Race|
|Drivetrain|FF cars only|
|Tires|Standard or Sports Tires required|
|Cars Permitted|All cars permitted|
|Required License|None|

|||||
|---|---|---|---|
|1st|1,500 cr|4th|300 cr|
|2nd|500 cr|5th|200 cr|
|3rd|400 cr|6th|100 cr|

- [ ] 2001 Mazda Mazda6 Concept

  - [ ] `[ | | | | | | | ]` 2 laps - Mid-Field Reverse
  - [ ] `[ | | | | | | | ]` 3 laps - Suzuka Circuit (East Course)
  - [ ] `[ | | | | | | | ]` 2 laps - Hong Kong Reverse
  - [ ] `[ | | | | | | | ]` 2 laps - Grand Valley East
  - [ ] `[ | | | | | | | ]` 2 laps - Twin Ring Motegi (East Short Course)

### 5.3 FR Challenge

|||
|---|---|
|Game Mode|Single Race|
|Drivetrain|FR cars only|
|Tires|Standard or Sports Tires required|
|Cars Permitted|All cars permitted|
|Required License|None|

|||||
|---|---|---|---|
|1st|1,500 cr|4th|300 cr|
|2nd|500 cr|5th|200 cr|
|3rd|400 cr|6th|100 cr|

- [ ] 1967 Nissan Skyline 2000GT-B (S54A)

  - [ ] `[ | | | | | | | ]` 2 laps - Seattle Circuit Reverse
  - [ ] `[ | | | | | | | ]` 3 laps - Tsukuba Circuit
  - [ ] `[ | | | | | | | ]` 2 laps - Special Stage Route 5
  - [ ] `[ | | | | | | | ]` 2 laps - Mazda Raceway Laguna Seca
  - [ ] `[ | | | | | | | ]` 4 laps - Motorland Reverse

### 5.4 4WD Challenge

|||
|---|---|
|Game Mode|Single Race|
|Drivetrain|4WD cars only|
|Tires|Standard or Sports Tires required|
|Cars Permitted|All cars permitted|
|Required License|None|

|||||
|---|---|---|---|
|1st|1,500 cr|4th|300 cr|
|2nd|500 cr|5th|200 cr|
|3rd|400 cr|6th|100 cr|

- [ ] 2004 Toyota Motor Triathlon Race Car

  - [ ] `[ | | | | | | | ]` 2 laps - Grand Valley East
  - [ ] `[ | | | | | | | ]` 4 laps - Autumn Ring Mini Reverse
  - [ ] `[ | | | | | | | ]` 3 laps - Suzuka Circuit (East Course)
  - [ ] `[ | | | | | | | ]` 2 laps - El Capitan Reverse
  - [ ] `[ | | | | | | | ]` 2 laps - Fuji Speedway 90's

### 5.5 MR Challenge

|||
|---|---|
|Game Mode|Single Race|
|Drivetrain|MR cars only|
|Tires|Standard or Sports Tires required|
|Cars Permitted|All cars permitted|
|Required License|None|

|||||
|---|---|---|---|
|1st|2,000 cr|4th|400 cr|
|2nd|1,000 cr|5th|300 cr|
|3rd|500 cr|6th|200 cr|

- [ ] 1987 Lotus Esprit Turbo HC

  - [ ] `[ | | | | | | | ]` 5 laps - Beginner Course
  - [ ] `[ | | | | | | | ]` 2 laps - Autumn Ring Reverse
  - [ ] `[ | | | | | | | ]` 2 laps - New York Reverse
  - [ ] `[ | | | | | | | ]` 2 laps - Fuji Speedway 90's
  - [ ] `[ | | | | | | | ]` 2 laps - El Capitan

### 5.6 Light-weight K-Car Cup

|||
|---|---|
|Game Mode|Single Race|
|Tires|Standard or Sports Tires required|
|Length Limit|3400 mm or less|
|Cars Permitted|All cars permitted|
|Required License|None|

|||||
|---|---|---|---|
|1st|2,000 cr|4th|300 cr|
|2nd|500 cr|5th|200 cr|
|3rd|400 cr|6th|100 cr|

- [ ] 1964 Ginetta G4

  - [ ] `[ | | | | | | | ]` 4 laps - Motorland
  - [ ] `[ | | | | | | | ]` 3 laps - Tsukuba Circuit
  - [ ] `[ | | | | | | | ]` 7 laps - Beginner Course

### 5.7 Spider & Roadster

|||
|---|---|
|Game Mode|Single Race|
|Car Type|Convertibles Only|
|Tires|Standard or Sports Tires required|
|Cars Permitted|All cars permitted|
|Required License|A-Spec: National B License|

|||||
|---|---|---|---|
|1st|3,000 cr|4th|400 cr|
|2nd|1,000 cr|5th|300 cr|
|3rd|500 cr|6th|200 cr|

- [ ] 2002 Chrysler Prowler

  - [ ] `[ | | | | | | | ]` 2 laps - Twin Ring Motegi (East Short Course)
  - [ ] `[ | | | | | | | ]` 2 laps - Trial Mountain Reverse
  - [ ] `[ | | | | | | | ]` 2 laps - Infineon Raceway (Sports Car Course)

### 5.8 Sport Truck Race

|||
|---|---|
|Game Mode|Single Race|
|Tires|Standard or Sports Tires required|
|Cars Permitted|Dodge RAM 1500 Larame Hemi|
||Toyota Tacoma X-Runner|
||Ford SVT F-150 Lightning|
||Chevrolet SSR|
||Chevrolet Silverado SST Concept|
|Required License|A-Spec: National B License|

|||||
|---|---|---|---|
|1st|1,200 cr|4th|400 cr|
|2nd|600 cr|5th|300 cr|
|3rd|500 cr|6th|200 cr|

- [ ] 2002 Chevrolet Silverado SST Concept

  - [ ] `[ | | | | | | | ]` 2 laps - Fuji Speedway 90's
  - [ ] `[ | | | | | | | ]` 2 laps - Mazda Raceway Laguna Seca
  - [ ] `[ | | | | | | | ]` 2 laps - Seattle Circuit Reverse

-------------------------------------------------------------------------------
## 6 Professional Events
-------------------------------------------------------------------------------

### 6.1 Clubman Cup

|||
|---|---|
|Game Mode|Single Race|
|Tires|Standard or Sports Tires required|
|Cars Permitted|All cars permitted|
|Required License|A-Spec: National B License|

|||||
|---|---|---|---|
|1st|3,000 cr|4th|400 cr|
|2nd|1,000 cr|5th|300 cr|
|3rd|500 cr|6th|200 cr|

- [ ] 2005 Mazda Mazdaspeed 6

  - [ ] `[ | | | | | | | ]` 3 laps - Apricot Hill Raceway
  - [ ] `[ | | | | | | | ]` 3 laps - Twin Ring Motegi (East Short Course)
  - [ ] `[ | | | | | | | ]` 4 laps - Seoul Central Reverse
  - [ ] `[ | | | | | | | ]` 5 laps - Clubman Stage Route 5
  - [ ] `[ | | | | | | | ]` 3 laps - Deep Forest Raceway

### 6.2 Tuning Car Grand Prix

|||
|---|---|
|Game Mode|Championship Race|
|Tires|No restrictions|
|Cars Permitted|All cars permitted|
|Required License|A-Spec: National A License|

|||||
|---|---|---|---|
|1st|10,000 cr|4th|3,000 cr|
|2nd|6,000 cr|5th|1,000 cr|
|3rd|4,000 cr|6th|500 cr|

|||
|---|---|
|Winning Prize|20,000 cr|

- [ ] 2004 Nissan Option Stream Z

  - [ ] `[ | | | | | | | ]` 3 laps - Apricot Hill Raceway Reverse
  - [ ] `[ | | | | | | | ]` 3 laps - Fuji Speedway 90's
  - [ ] `[ | | | | | | | ]` 2 laps - Tokyo Route 246
  - [ ] `[ | | | | | | | ]` 3 laps - El Capitan Reverse
  - [ ] `[ | | | | | | | ]` 5 laps - Tsukuba Circuit

### 6.3 Race of NA Sports

|||
|---|---|
|Game Mode|Single Race|
|Engine Type|NA engines only|
|Tires|Standard or Sports Tires required|
|Cars Permitted|All cars permitted|
|Required License|A-Spec: National B License|

|||||
|---|---|---|---|
|1st|7,500 cr|4th|1,000 cr|
|2nd|5,000 cr|5th|500 cr|
|3rd|2,500 cr|6th|300 cr|

- [ ] 2001 Honda NSX-R Concept

  - [ ] `[ | | | | | | | ]` 3 laps - Infineon Raceway (Sports Car Course)
  - [ ] `[ | | | | | | | ]` 3 laps - Apricot Hill Raceway Reverse
  - [ ] `[ | | | | | | | ]` 3 laps - Twin Ring Motegi (Road Course)
  - [ ] `[ | | | | | | | ]` 3 laps - Special Stage Route 5 Reverse
  - [ ] `[ | | | | | | | ]` 3 laps - Trial Mountain Circuit

### 6.4 Race of Turbo Sports

|||
|---|---|
|Game Mode|Single Race|
|Engine Type|Turbo engines only|
|Tires|Standard or Sports Tires required|
|Cars Permitted|All cars permitted|
|Required License|A-Spec: National B License|

|||||
|---|---|---|---|
|1st|7,500 cr|4th|1,000 cr|
|2nd|5,000 cr|5th|500 cr|
|3rd|2,500 cr|6th|300 cr|

- [ ] 2003 Mazda BP Falken RX-7 (D1GP)

  - [ ] `[ | | | | | | | ]` 3 laps - Fuji Speedway 80's
  - [ ] `[ | | | | | | | ]` 2 laps - Tokyo Route 246 Reverse
  - [ ] `[ | | | | | | | ]` 3 laps - High-Speed Ring
  - [ ] `[ | | | | | | | ]` 3 laps - New York Reverse
  - [ ] `[ | | | | | | | ]` 3 laps - Mid-Field Raceway

### 6.5 Boxer Spirit

|||
|---|---|
|Game Mode|Single Race|
|Tires|Standard or Sports Tires required|
|Cars Permitted|Subaru Impreza Sedan WRX STi spec C|
||Subaru Legacy B4 2.0GT spec B|
||RUF RGT|
||RUF 3400S|
||VW Karmann Ghia Coupe (Type-1)|
||etc.||
|Required License|A-Spec: National B License|

|||||
|---|---|---|---|
|1st|7,500 cr|4th|1,000 cr|
|2nd|5,000 cr|5th|500 cr|
|3rd|2,500 cr|6th|300 cr|

- [ ] 1987 RUF CTR Yellow Bird

  - [ ] `[ | | | | | | | ]` 4 laps - Hong Kong
  - [ ] `[ | | | | | | | ]` 3 laps - Infineon Raceway (Sports Car Course)
  - [ ] `[ | | | | | | | ]` 3 laps - Deep Forest Raceway Reverse

### 6.6 World Classic Car Series

|||
|---|---|
|Game Mode|Championship Race|
|Tires|Standard or Sports Tires required|
|Model Year Restrictions|Up to 1970|
|Cars Permitted|All cars permitted|
|Required License|A-Spec: International B License|

|||||
|---|---|---|---|
|1st|10,000 cr|4th|3,000 cr|
|2nd|6,000 cr|5th|1,000 cr|
|3rd|4,000 cr|6th|500 cr|

|||
|---|---|
|Winning Prize|50,000 cr|

- [ ] 1886 Mercedes-Benz Daimler Motor Carriage

  - [ ] `[ | | | | | | | ]` 2 laps - Fuji Speedway 80's
  - [ ] `[ | | | | | | | ]` 2 laps - El Capitan Reverse
  - [ ] `[ | | | | | | | ]` 1 lap  - Nurburgring Nordschleife
  - [ ] `[ | | | | | | | ]` 2 laps - Cote d' Azur
  - [ ] `[ | | | | | | | ]` 2 laps - Mazda Raceway Laguna Seca

### 6.7 World Compact Car Race

|||
|---|---|
|Game Mode|Championship Race|
|Tires|Standard or Sports Tires required|
|Length Limit|4000 mm or less|
|Cars Permitted|All cars permitted|
|Required License|A-Spec: International B License|

|||||
|---|---|---|---|
|1st|7,500 cr|4th|1,000 cr|
|2nd|5,000 cr|5th|500 cr|
|3rd|2,500 cr|6th|300 cr|

|||
|---|---|
|Winning Prize|30,000 cr|

- [ ] 1968 Honda S800 RSC Race Car

  - [ ] `[ | | | | | | | ]` 3 laps - Seattle Circuit
  - [ ] `[ | | | | | | | ]` 5 laps - Tsukuba Circuit
  - [ ] `[ | | | | | | | ]` 4 laps - Grand Valley East Reverse
  - [ ] `[ | | | | | | | ]` 4 laps - Hong Kong Reverse
  - [ ] `[ | | | | | | | ]` 4 laps - Twin Ring Motegi (East Short Course)

### 6.8 Supercar Festival

|||
|---|---|
|Game Mode|Single Race|
|Car Type|Production cars only|
|Tires|Standard or Sports Tires required|
|Power Restrictions|493 HP or over|
|Cars Permitted|All cars permitted|
|Required License|A-Spec: International A License|

|||||
|---|---|---|---|
|1st|15,000 cr|4th|4,000 cr|
|2nd|7,500 cr|5th|1,500 cr|
|3rd|5,000 cr|6th|800 cr|

- [ ] 1994 Cizeta V16T

  - [ ] `[ | | | | | | | ]` 8 laps - Seoul Central
  - [ ] `[ | | | | | | | ]` 6 laps - Fuji Speedway 90's
  - [ ] `[ | | | | | | | ]` 6 laps - New York
  - [ ] `[ | | | | | | | ]` 6 laps - Mid-Field Raceway Reverse
  - [ ] `[ | | | | | | | ]` 6 laps - Infineon Raceway (Sports Car Course)

### 6.9 Gran Turismo World Championship

|||
|---|---|
|Game Mode|Championship Race|
|Tires|No restrictions|
|Cars Permitted|All cars permitted|
|Required License|A-Spec: International A License|

|||||
|---|---|---|---|
|1st|25,000 cr|4th|4,000 cr|
|2nd|7,500 cr|5th|1,500 cr|
|3rd|5,000 cr|6th|800 cr|

|||
|---|---|
|Winning Prize|250,000 cr|

- [ ] 2004 Ford GT LM Race Car Spec-II

  - [ ] `[ | | | | | | | ]` 10 laps - Tokyo Route 24
  - [ ] `[ | | | | | | | ]` 21 laps - Twin Ring Motegi (Super Speedway)
  - [ ] `[ | | | | | | | ]` 18 laps - Hong Kong
  - [ ] `[ | | | | | | | ]` 19 laps - Seoul Central Reverse
  - [ ] `[ | | | | | | | ]` 11 laps - El Capitan
  - [ ] `[ | | | | | | | ]` 15 laps - New York
  - [ ] `[ | | | | | | | ]` 18 laps - Opera Paris Reverse
  - [ ] `[ | | | | | | | ]`  9 laps - Suzuka Circuit
  - [ ] `[ | | | | | | | ]` 11 laps - Grand Valley Speedway Reverse
  - [ ] `[ | | | | | | | ]`  4 laps - Circuit de la Sarthe I

-------------------------------------------------------------------------------
## 7 Extreme Events
-------------------------------------------------------------------------------

### 7.1 Gran Turismo All Stars

|||
|---|---|
|Game Mode|Championship Race|
|Tires|No restrictions|
|Cars Permitted|All cars permitted|
|Required License|A-Spec: International A License|

|||||
|---|---|---|---|
|1st|15,000 cr|4th|4,000 cr|
|2nd|7,500 cr|5th|1,500 cr|
|3rd|5,000 cr|6th|800 cr|

|||
|---|---|
|Winning Prize|100,000 cr|

- [ ] 1997 BMW BMW McLaren F1 GTR Race Car

  - [ ] `[ | | | | | | | ]` 7 laps - High-Speed Ring Reverse
  - [ ] `[ | | | | | | | ]` 6 laps - Fuji Speedway 80's
  - [ ] `[ | | | | | | | ]` 7 laps - Mazda Raceway Laguna Seca
  - [ ] `[ | | | | | | | ]` 9 laps - Autumn Ring Reverse
  - [ ] `[ | | | | | | | ]` 3 laps - Test Course
  - [ ] `[ | | | | | | | ]` 6 laps - Grand Valley Speedway Reverse
  - [ ] `[ | | | | | | | ]` 5 laps - Suzuka Circuit
  - [ ] `[ | | | | | | | ]` 8 laps - Infineon Raceway (Stock Car Course)
  - [ ] `[ | | | | | | | ]` 2 laps - Circuit de la Sarthe I
  - [ ] `[ | | | | | | | ]` 2 laps - Nurburgring Nordschleife

### 7.2 Dream Car Championship

|||
|---|---|
|Game Mode|Championship Race|
|Tires|No restrictions|
|Cars Permitted|All cars permitted|
|Required License|A-Spec: Super License|

|||||
|---|---|---|---|
|1st|20,000 cr|4th|5,000 cr|
|2nd|8,000 cr|5th|2,000 cr|
|3rd|6,000 cr|6th|1,000 cr|

|||
|---|---|
|Winning Prize|200,000 cr|

- [ ] 2002 Nissan GT-R Concept LM Race Car

  - [ ] `[ | | | | | | | ]`  7 laps - Opera Paris
  - [ ] `[ | | | | | | | ]`  4 laps - Tokyo Route 246 Reverse
  - [ ] `[ | | | | | | | ]`  5 laps - Deep Forest Raceway
  - [ ] `[ | | | | | | | ]`  7 laps - Seoul Central Reverse
  - [ ] `[ | | | | | | | ]`  7 laps - Hong Kong
  - [ ] `[ | | | | | | | ]`  2 laps - Test Course
  - [ ] `[ | | | | | | | ]` 27 laps - Beginner Course
  - [ ] `[ | | | | | | | ]`  2 laps - Circuit de la Sarthe II
  - [ ] `[ | | | | | | | ]`  4 laps - El Capitan Reverse
  - [ ] `[ | | | | | | | ]`  7 laps - Cote d' Azur

### 7.3 Polyphony Digital Cup

|||
|---|---|
|Game Mode|Championship Race|
|Tires|Standard or Sports Tires required|
|Cars Permitted|All cars permitted|
|Required License|A-Spec: Super License|

|||||
|---|---|---|---|
|1st|20,000 cr|4th|5,000 cr|
|2nd|8,000 cr|5th|2,000 cr|
|3rd|6,000 cr|6th|1,000 cr|

|||
|---|---|
|Winning Prize|200,000 cr|

- [ ] 2004 Opera Performance Opera Performance S2000

  - [ ] `[ | | | | | | | ]`  6 laps - Twin Ring Motegi (Road Course)
  - [ ] `[ | | | | | | | ]`  7 laps - Seattle Circuit Reverse
  - [ ] `[ | | | | | | | ]`  8 laps - Infineon Raceway (Stock Car Course)
  - [ ] `[ | | | | | | | ]`  5 laps - Tokyo Route 246 Reverse
  - [ ] `[ | | | | | | | ]`  6 laps - Fuji Speedway 2005
  - [ ] `[ | | | | | | | ]` 19 laps - Motorland Reverse
  - [ ] `[ | | | | | | | ]`  2 laps - Circuit de la Sarthe I

  - [ ] `[ | | | | | | | ]`  6 laps - El Capitan Reverse
  - [ ] `[ | | | | | | | ]`  5 laps - Suzuka Circuit
  - [ ] `[ | | | | | | | ]`  2 laps - Nurburgring Nordschleife

### 7.4 Like the Wind

|||
|---|---|
|Game Mode|Single Race|
|Tires|No restrictions|
|Cars Permitted|All cars permitted|
|Required License|A-Spec: Super License|

|||||
|---|---|---|---|
|1st|20,000 cr|4th|5,000 cr|
|2nd|8,000 cr|5th|2,000 cr|
|3rd|6,000 cr|6th|1,000 cr|

- [ ] 2001 Volkswagen W12 Nardo Concept

  - [ ] `[ | | | | | | | ]` 5 laps - Test Course

### 7.5 Formula GT World Championship

|||
|---|---|
|Game Mode|Championship Race|
|Tires|No restrictions|
|Cars Permitted|All cars permitted|
|Required License|A-Spec: Super License|

|||||
|---|---|---|---|
|1st|100,000 cr|4th|5,000 cr|
|2nd|25,000 cr|5th|2,000 cr|
|3rd|10,000 cr|6th|1,000 cr|

|||
|---|---|
|Winning Prize|3,000,000 cr|

- [ ] 1989 Mercedes-Benz Sauber C9 Race Car

  - [ ] `[ | | | | | | | ]`  60 laps - Tokyo Route 246
  - [ ] `[ | | | | | | | ]` 127 laps - Twin Ring Motegi (Super Speedway)
  - [ ] `[ | | | | | | | ]`  74 laps - New York
  - [ ] `[ | | | | | | | ]`  77 laps - High-Speed Ring
  - [ ] `[ | | | | | | | ]`  62 laps - Grand Valley Speedway
  - [ ] `[ | | | | | | | ]`  23 laps - Circuit de la Sarthe I
  - [ ] `[ | | | | | | | ]`  78 laps - Cote d' Azur
  - [ ] `[ | | | | | | | ]` 113 laps - Seoul Central
  - [ ] `[ | | | | | | | ]`  76 laps - Infineon Raceway (Sports Car Course)
  - [ ] `[ | | | | | | | ]`  85 laps - Mazda Raceway Laguna Seca
  - [ ] `[ | | | | | | | ]`  64 laps - Twin Ring Motegi (Road Course)
  - [ ] `[ | | | | | | | ]`  15 laps - Nurburgring Nordschleife
  - [ ] `[ | | | | | | | ]`  64 laps - El Capitan
  - [ ] `[ | | | | | | | ]`  67 laps - Fuji Speedway 2005
  - [ ] `[ | | | | | | | ]`  53 laps - Suzuka Circuit

### 7.6 World Circuit Tours

|||
|---|---|
|Game Mode|Single Race|
|Tires|No restrictions|
|Cars Permitted|All cars permitted|
|Required License|A-Spec: International A License|

|||||
|---|---|---|---|
|1st|15,000 cr|4th|4,000 cr|
|2nd|7,500 cr|5th|1,500 cr|
|3rd|5,000 cr|6th|800 cr|

- [ ] 2001 Nissan Gran Turismo Skyline GT-R (Pace Car)

  - [ ] `[ | | | | | | | ]`  6 laps - Suzuka Circuit
  - [ ] `[ | | | | | | | ]`  8 laps - Twin Ring Motegi (Road Course)
  - [ ] `[ | | | | | | | ]` 15 laps - Tsukuba Circuit
  - [ ] `[ | | | | | | | ]` 10 laps - Mazda Raceway Laguna Seca
  - [ ] `[ | | | | | | | ]`  9 laps - Infineon Raceway (Sports Car Course)
  - [ ] `[ | | | | | | | ]`  8 laps - Fuji Speedway 2005
  - [ ] `[ | | | | | | | ]`  3 laps - Circuit de la Sarthe I
  - [ ] `[ | | | | | | | ]`  2 laps - Nurburgring Nordschleife

### 7.7 Premium Sports Lounge

|||
|---|---|
|Game Mode|Single Race|
|Car Type|Production cars only|
|Tires|Standard or Sports Tires required|
|Cars Permitted|Mercedes SLR McLaren|
||Aston Martin Vanquish|
||Fort GT|
||BMW M3 GTR|
||Lotus Esprit V8|
||etc.|
|Required License|A-Spec: International B License|

|||||
|---|---|---|---|
|1st|12,000 cr|4th|4,000 cr|
|2nd|6,500 cr|5th|1,500 cr|
|3rd|5,000 cr|6th|800 cr|

- [ ] 2005 Ford Ford GT

  - [ ] `[ | | | | | | | ]` 4 laps - Cote d' Azur
  - [ ] `[ | | | | | | | ]` 5 laps - Opera Paris
  - [ ] `[ | | | | | | | ]` 5 laps - Hong Kong Reverse
  - [ ] `[ | | | | | | | ]` 4 laps - High-Speed Ring Reverse
  - [ ] `[ | | | | | | | ]` 3 laps - New York Reverse

-------------------------------------------------------------------------------
## 8 Endurance Events
-------------------------------------------------------------------------------

### 8.1 Grand Valley 300 km

|||
|---|---|
|Game Mode|Single Race|
|Tires|No restrictions|
|Cars Permitted|All cars permitted|
|Required License|A-Spec: International B License|

|||||
|---|---|---|---|
|1st|400,000 cr|4th|65,000 cr|
|2nd|100,000 cr|5th|40,000 cr|
|3rd|85,000 cr|6th|11,000 cr|

- [ ] 1937 Audi Auto Union V16 Type C Streamline

  - [ ] `[ | | | | | | | ]` 60 laps - Grand Valley Speedway

### 8.2 Laguna Seca 200 miles

|||
|---|---|
|Game Mode|Single Race|
|Tires|No restrictions|
|Cars Permitted|All cars permitted|
|Required License|A-Spec: National A License|

|||||
|---|---|---|---|
|1st|200,000 cr|4th|50,000 cr|
|2nd|80,000 cr|5th|25,000 cr|
|3rd|65,000 cr|6th|9,500 cr|

- [ ] 1969 Ford GT40 Race Car

  - [ ] `[ | | | | | | | ]` 90 laps - Mazda Raceway Laguna Seca

### 8.3 Roadster 4h Endurance

|||
|---|---|
|Game Mode|Single Race|
|Tires|Standard or Sports Tires required|
|Cars Permitted|Mazda MX-5 Miata 1800 RS (J)|
||Mazda MX-5 Miata 1600 NR-A (J)|
||Mazda MX-5 Miata 1.8 RS (J)|
||Mazda MX-5 Miata VR-Limited (J)|
||etc.|
|Required License|None|

|||||
|---|---|---|---|
|1st|100,000 cr|4th|35,000 cr|
|2nd|60,000 cr|5th|15,000 cr|
|3rd|45,000 cr|6th|8,500 cr|

- [ ] 2001 Mazda RX-7 LM Race Car

  - [ ] `[ | | | | | | | ]` 4 hours - Tsukuba Circuit

### 8.4 Tokyo R246 300 km

|||
|---|---|
|Game Mode|Single Race|
|Tires|No restrictions|
|Cars Permitted|All cars permitted|
|Required License|A-Spec: International B License|

|||||
|---|---|---|---|
|1st|200,000 cr|4th|50,000 cr|
|2nd|80,000 cr|5th|25,000 cr|
|3rd|65,000 cr|6th|9,500 cr|

- [ ] 1997 Mitsubishi FTO Super Touring Car

  - [ ] `[ | | | | | | | ]` 60 laps - Tokyo Route 246

### 8.5 Super Speedway 150 miles

|||
|---|---|
|Game Mode|Single Race|
|Tires|No restrictions|
|Cars Permitted|All cars permitted|
|Required License|A-Spec: International B License|

|||||
|---|---|---|---|
|1st|200,000 cr|4th|50,000 cr|
|2nd|80,000 cr|5th|25,000 cr|
|3rd|65,000 cr|6th|9,500 cr|

- [ ] 2002 Honda NSX-R Prototype LM Race Car

  - [ ] `[ | | | | | | | ]` 100 laps - Twin Ring Motegi (Super Speedway)

### 8.6 Nurburgring 24h Endurance

|||
|---|---|
|Game Mode|Single Race|
|Tires|No restrictions|
|Cars Permitted|All cars permitted|
|Required License|A-Spec: International B License|

|||||
|---|---|---|---|
|1st|1,200,000 cr|4th|150,000 cr|
|2nd|500,000 cr|5th|120,000 cr|
|3rd|300,000 cr|6th|100,000 cr|

- [ ] 2004 Polyphony Digital Formula Gran Turismo

  - [ ] `[ | | | | | | | ]` 24 hours - Nurburgring Nordschleife

### 8.7 Nurburgring 4h Endurance

|||
|---|---|
|Game Mode|Single Race|
|Tires|Standard or Sports Tires required|
|Cars Permitted|All cars permitted|
|Required License|A-Spec: International B License|

|||||
|---|---|---|---|
|1st|200,000 cr|4th|50,000 cr|
|2nd|80,000 cr|5th|25,000 cr|
|3rd|65,000 cr|6th|9,500 cr|

- [ ] 1967 Chaparral 2D Race Car

  - [ ] `[ | | | | | | | ]` 4 hours - Nurburgring Nordschleife

### 8.8 Suzuka 1000 km

|||
|---|---|
|Game Mode|Single Race|
|Tires|Standard or Sports Tires required|
|Cars Permitted|All cars permitted|
|Required License|A-Spec: International B License|

|||||
|---|---|---|---|
|1st|300,000 cr|4th|60,000 cr|
|2nd|130,000 cr|5th|30,000 cr|
|3rd|70,000 cr|6th|10,000 cr|

- [ ] 1999 Lister Storm V12 Race Car

  - [ ] `[ | | | | | | | ]` 172 laps - Suzuka Circuit

### 8.9 Motegi 8h Endurance

|||
|---|---|
|Game Mode|Single Race|
|Tires|Standard or Sports Tires required|
|Cars Permitted|All cars permitted|
|Required License|A-Spec: National A License|

|||||
|---|---|---|---|
|1st|120,000 cr|4th|40,000 cr|
|2nd|60,000 cr|5th|15,000 cr|
|3rd|50,000 cr|6th|7,000 cr|

- [ ] 2002 Honda NSX-R Prototype LM Road Car

  - [ ] `[ | | | | | | | ]` 8 hours - Twin Ring Motegi (Road Course)

### 8.10 Tsukuba 9h Endurance

|||
|---|---|
|Game Mode|Single Race|
|Tires|Standard or Sports Tires required|
|Cars Permitted|All cars permitted|
|Required License|A-Spec: National B License|

|||||
|---|---|---|---|
|1st|150,000 cr|4th|45,000 cr|
|2nd|70,000 cr|5th|20,000 cr|
|3rd|55,000 cr|6th|9,000 cr|

- [ ] 1993 Nissan Calsonic Skyline GT-R Race Car

  - [ ] `[ | | | | | | | ]` 9 hours - Tsukuba Circuit

### 8.11 Circuit de la Sarthe 24h I

|||
|---|---|
|Game Mode|Single Race|
|Tires|No restrictions|
|Cars Permitted|All cars permitted|
|Required License|A-Spec: International A License|

|||||
|---|---|---|---|
|1st|1,200,000 cr|4th|150,000 cr|
|2nd|500,000 cr|5th|120,000 cr|
|3rd|300,000 cr|6th|100,000 cr|

- [ ] 2001 Audi R8 Race Car

  - [ ] `[ | | | | | | | ]` 24 hours - Circuit de la Sarthe I

### 8.12 Circuit de la Sarthe 24h II

|||
|---|---|
|Game Mode|Single Race|
|Tires|No restrictions|
|Cars Permitted|All cars permitted|
|Required License|A-Spec: International A License|

|||||
|---|---|---|---|
|1st|1,000,000 cr|4th|120,000 cr|
|2nd|400,000 cr|5th|100,000 cr|
|3rd|250,000 cr|6th|80,000 cr|

- [ ] 2003 Bentley Speed 8 Race Car

  - [ ] `[ | | | | | | | ]` 24 hours - Circuit de la Sarthe II

### 8.13 Fuji 1000 km

|||
|---|---|
|Game Mode|Single Race|
|Tires|No restrictions|
|Cars Permitted|All cars permitted|
|Required License|A-Spec: International A License|

|||||
|---|---|---|---|
|1st|750,000 cr|4th|100,000 cr|
|2nd|250,000 cr|5th|80,000 cr|
|3rd|120,000 cr|6th|40,000 cr|

- [ ] 1992 Nissan R92CP Race Car

  - [ ] `[ | | | | | | | ]` 228 laps - Fuji Speedway 90's

### 8.14 Infineon World Sports

|||
|---|---|
|Game Mode|Single Race|
|Tires|No restrictions|
|Cars Permitted|All cars permitted|
|Required License|A-Spec: International A License|

|||||
|---|---|---|---|
|1st|500,000 cr|4th|80,000 cr|
|2nd|120,000 cr|5th|50,000 cr|
|3rd|100,000 cr|6th|12,000 cr|

- [ ] 2002 Ford Ford GT LM Race Car

  - [ ] `[ | | | | | | | ]` 2.75 hours - Infineon Raceway (Sports Car Course)

### 8.15 El Capitan 200 miles

|||
|---|---|
|Game Mode|Single Race|
|Tires|Standard or Sports Tires required|
|Cars Permitted|All cars permitted|
|Required License|A-Spec: National A License|

|||||
|---|---|---|---|
|1st|250,000 cr|4th|65,000 cr|
|2nd|100,000 cr|5th|40,000 cr|
|3rd|85,000 cr|6th|11,000 cr|

- [ ] 1989 Toyota Minolta Toyota 88C-V Race Car

  - [ ] `[ | | | | | | | ]` 66 laps - El Capitan

### 8.16 New York 200 miles

|||
|---|---|
|Game Mode|Single Race|
|Tires|Standard or Sports Tires required|
|Cars Permitted|All cars permitted|
|Required License|A-Spec: National A License|

|||||
|---|---|---|---|
|1st|250,000 cr|4th|65,000 cr|
|2nd|100,000 cr|5th|40,000 cr|
|3rd|85,000 cr|6th|11,000 cr|

- [ ] 1971 Dodge Charger Super Bee 426 Hemi

  - [ ] `[ | | | | | | | ]` 76 laps - New York

-------------------------------------------------------------------------------
## 9 American Events
-------------------------------------------------------------------------------

### 9.1 All American Championship

|||
|---|---|
|Game Mode|Championship Race|
|Tires|No restrictions|
|Country Restrictions|US cars only|
|Cars Permitted|All cars permitted|
|Required License|A-Spec: National A License|

|||||
|---|---|---|---|
|1st|10,000 cr|4th|3,000 cr|
|2nd|6,000 cr|5th|1,000 cr|
|3rd|4,000 cr|6th|500 cr|

|||
|---|---|
|Winning Prize|35,000 cr|

- [ ] 1954 Chevrolet Corvette Convertible (C1)

  - [ ] `[ | | | | | | | ]` 5 laps - Seattle Circuit
  - [ ] `[ | | | | | | | ]` 5 laps - Infineon Raceway (Sports Car Course)
  - [ ] `[ | | | | | | | ]` 4 laps - New York
  - [ ] `[ | | | | | | | ]` 4 laps - El Capitan Reverse
  - [ ] `[ | | | | | | | ]` 5 laps - Mazda Raceway Laguna Raceway

### 9.2 Stars and Stripes

|||
|---|---|
|Game Mode|Single Race|
|Car Type|Production cars only|
|Tires|Standard or Sports Tires required|
|Country Restrictions|US cars only|
|Cars Permitted|All cars permitted|
|Required License|A-Spec: International B License|

|||||
|---|---|---|---|
|1st|10,000 cr|4th|3,000 cr|
|2nd|6,000 cr|5th|1,000 cr|
|3rd|4,000 cr|6th|500 cr|

- [ ] 2001 Chevrolet Camaro LM Race Car

  - [ ] `[ | | | | | | | ]` 3 laps - Mazda Raceway Laguna Seca
  - [ ] `[ | | | | | | | ]` 3 laps - Seattle Circuit
  - [ ] `[ | | | | | | | ]` 4 laps - Infineon Raceway (Stock Car Course)

### 9.3 Hot Rod Competition

|||
|---|---|
|Game Mode|Single Race|
|Tires|Standard or Sports Tires required|
|Country Restrictions|US cars only|
|Cars Permitted|All cars permitted|
|Required License|A-Spec: National A License|

|||||
|---|---|---|---|
|1st|3,000 cr|4th|400 cr|
|2nd|1,000 cr|5th|300 cr|
|3rd|500 cr|6th|200 cr|

- [ ] 1970 Chevrolet Chevelle SS 454

  - [ ] `[ | | | | | | | ]` 3 laps - New York Reverse
  - [ ] `[ | | | | | | | ]` 3 laps - Mazda Raceway Laguna Seca
  - [ ] `[ | | | | | | | ]` 3 laps - El Capitan

### 9.4 Muscle Car Competition

|||
|---|---|
|Game Mode|Single Race|
|Tires|Standard or Sports Tires required|
|Country Restrictions|US cars only|
|Model Year Restrictions|Up to 1980|
|Cars Permitted|All cars permitted|
|Required License|A-Spec: National B License|

|||||
|---|---|---|---|
|1st|3,000 cr|4th|400 cr|
|2nd|1,000 cr|5th|300 cr|
|3rd|500 cr|6th|200 cr|

- [ ] 1970 Plymouth Super Bird

  - [ ] `[ | | | | | | | ]` 2 laps - New York
  - [ ] `[ | | | | | | | ]` 2 laps - Infineon Raceway (Sports Car Course)
  - [ ] `[ | | | | | | | ]` 2 laps - Seattle Circuit

-------------------------------------------------------------------------------
## 10 European Events
-------------------------------------------------------------------------------

### 10.1 Pan Euro Championship

|||
|---|---|
|Game Mode|Championship Race|
|Tires|Standard or Sports Tires required|
|Country Restrictions|European cars only|
|Cars Permitted|All cars permitted|
|Required License|A-Spec: International B License|

|||||
|---|---|---|---|
|1st|12,000 cr|4th|4,000 cr|
|2nd|6,500 cr|5th|1,500 cr|
|3rd|5,000 cr|6th|800 cr|

|||
|---|---|
|Winning Prize|45,000 cr|

- [ ] 2001 Jaguar XJ220 LM Race Car

  - [ ] `[ | | | | | | | ]` 6 laps - Opera Paris
  - [ ] `[ | | | | | | | ]` 5 laps - Grand Valley Speedway
  - [ ] `[ | | | | | | | ]` 2 laps - Circuit de la Sarthe I
  - [ ] `[ | | | | | | | ]` 6 laps - Cote d' Azur
  - [ ] `[ | | | | | | | ]` 2 laps - Nurburgring Nordschleife

### 10.2 British GT Series

|||
|---|---|
|Game Mode|Championship Race|
|Tires|Standard or Sports Tires required|
|Country Restrictions|UK cars only|
|Cars Permitted|All cars permitted|
|Required License|A-Spec: International A License|

|||||
|---|---|---|---|
|1st|12,000 cr|4th|4,000 cr|
|2nd|6,500 cr|5th|1,500 cr|
|3rd|5,000 cr|6th|800 cr|

|||
|---|---|
|Winning Prize|45,000 cr|

- [ ] 1961 Jaguar E-Type Coupe

  - [ ] `[ | | | | | | | ]` 3 laps - Grand Valley Speedway
  - [ ] `[ | | | | | | | ]` 3 laps - Fuji Speedway 90's
  - [ ] `[ | | | | | | | ]` 3 laps - El Capitan
  - [ ] `[ | | | | | | | ]` 3 laps - Infineon Raceway (Sports Car Course)
  - [ ] `[ | | | | | | | ]` 3 laps - Mid-Field Raceway Reverse

### 10.3 British Lightweight Series

|||
|---|---|
|Game Mode|Single Race|
|Tires|Standard or Sports Tires required|
||Weight Limit: 1000 kg or less|
|Country Restrictions|UK cars only|
|Cars Permitted|All cars permitted|
|Required License|A-Spec: National B License|

|||||
|---|---|---|---|
|1st|3,000 cr|4th|400 cr|
|2nd|1,000 cr|5th|300 cr|
|3rd|500 cr|6th|200 cr|

- [ ] 1971 Lotus Europa Special

  - [ ] `[ | | | | | | | ]` 2 laps - Autumn Ring
  - [ ] `[ | | | | | | | ]` 2 laps - Apricot Hill Raceway Reverse
  - [ ] `[ | | | | | | | ]` 2 laps - Special Stage Route 5

### 10.4 Deutsche Touring Car Meisterschaft

|||
|---|---|
|Game Mode|Championship Race|
|Tires|No restrictions|
|Cars Permitted|Audi A4 Touring Car|
||Abt Audi TT-R Touring Car|
||Opel Astra Touring Car|
||Mercedes CLK Touring Car|
||Opel Calibra Touring Car|
||etc.|
|Required License|A-Spec: International A License|

|||||
|---|---|---|---|
|1st|15,000 cr|4th|4,000 cr|
|2nd|7,500 cr|5th|1,500 cr|
|3rd|5,000 cr|6th|800 cr|

|||
|---|---|
|Winning Prize|75,000 cr|

- [ ] 1998 Mercedes-Benz AMG CLK-GTR Race Car

  - [ ] `[ | | | | | | | ]` 6 laps - Opera Paris
  - [ ] `[ | | | | | | | ]` 6 laps - Mid-Field Raceway
  - [ ] `[ | | | | | | | ]` 5 laps - High-Speed Ring Reverse
  - [ ] `[ | | | | | | | ]` 1 lap  - Nurburgring Nordschleife
  - [ ] `[ | | | | | | | ]` 5 laps - Fuji Speedway 2005

### 10.5 La Festa Italiano

|||
|---|---|
|Game Mode|Single Race|
|Car Type|Production cars only|
|Tires|Standard or Sports Tires required|
|Country Restrictions|Italian cars only|
|Cars Permitted|All cars permitted|
|Required License|A-Spec: International B License|

|||||
|---|---|---|---|
|1st|5,000 cr|4th|500 cr|
|2nd|2,000 cr|5th|400 cr|
|3rd|1,000 cr|6th|300 cr|

- [ ] 1993 Alfa Romeo 155 2.5 V6 TI

  - [ ] `[ | | | | | | | ]` 2 laps - Autumn Ring Reverse
  - [ ] `[ | | | | | | | ]` 2 laps - Cote d' Azur
  - [ ] `[ | | | | | | | ]` 2 laps - Infineon Raceway (Sports Car Course)

### 10.6 Tous France Championnat

|||
|---|---|
|Game Mode|Championship Race|
|Car Type|Production cars only|
|Tires|Standard or Sports Tires required|
|Country Restrictions|French cars only|
|Cars Permitted|All cars permitted|
|Required License|A-Spec: National A License|

|||||
|---|---|---|---|
|1st|10,000 cr|4th|3,000 cr|
|2nd|6,000 cr|5th|1,000 cr|
|3rd|4,000 cr|6th|500 cr|

|||
|---|---|
|Winning Prize|35,000 cr|

- [ ] 1954 Citroen 2CV Type-A

  - [ ] `[ | | | | | | | ]` 4 laps - Opera Paris
  - [ ] `[ | | | | | | | ]` 1 lap  - Circuit de la Sarthe I
  - [ ] `[ | | | | | | | ]` 3 laps - Special Stage Route 5
  - [ ] `[ | | | | | | | ]` 3 laps - Grand Valley Speedway Reverse
  - [ ] `[ | | | | | | | ]` 3 laps - Cote d' Azur

### 10.7 Europe Classic Car League

|||
|---|---|
|Game Mode|Championship Race|
|Tires|Standard or Sports Tires required|
|Country Restrictions|European cars only|
|Model Year Restrictions|Up to 1970|
|Cars Permitted|All cars permitted|
|Required License|A-Spec: International B License|

|||||
|---|---|---|---|
|1st|10,000 cr|4th|3,000 cr|
|2nd|6,000 cr|5th|1,000 cr|
|3rd|4,000 cr|6th|500 cr|

|||
|---|---|
|Winning Prize|30,000 cr|

- [ ] 1886 Mercedes-Benz Patent Motor Wagen

  - [ ] `[ | | | | | | | ]` 1 lap  - Nurburgring Nordschleife
  - [ ] `[ | | | | | | | ]` 2 laps - Fuji Speedway 80's
  - [ ] `[ | | | | | | | ]` 2 laps - Opera Paris Reverse
  - [ ] `[ | | | | | | | ]` 1 lap  - Suzuka Circuit
  - [ ] `[ | | | | | | | ]` 2 laps - Deep Forest Raceway Reverse

### 10.8 Euro Hot Hatch League

|||
|---|---|
|Game Mode|Championship Race|
|Car Type|Production cars only|
|Tires|Standard or Sports Tires required|
|Cars Permitted|Alfa Romeo 147 GTA|
||Peugeot 206 RC|
||Clio Renault Sport 2.0 16V|
||Mini Cooper-S|
||VW Golf V GTI|
||etc.|
|Required License|A-Spec: International B License|

|||||
|---|---|---|---|
|1st|12,000 cr|4th|4,000 cr|
|2nd|6,500 cr|5th|1,500 cr|
|3rd|5,000 cr|6th|800 cr|

|||
|---|---|
|Winning Prize|45,000 cr|

- [ ] 1988 Volvo 240 GLT Estate

  - [ ] `[ | | | | | | | ]` 3 laps - Twin Ring Motegi (East Short Course)
  - [ ] `[ | | | | | | | ]` 4 laps - Opera Paris
  - [ ] `[ | | | | | | | ]` 3 laps - Trial Mountain Reverse
  - [ ] `[ | | | | | | | ]` 5 laps - Suzuka Circuit (East Course)
  - [ ] `[ | | | | | | | ]` 3 laps - Special Stage Route 5 Reverse

### 10.9 1000 miles !

|||
|---|---|
|Game Mode|Championship Race|
|Car Type|Production cars only|
|Tires|Standard or Sports Tires required|
|Model Year Restrictions|Up to 1970|
|Cars Permitted|All cars permitted|
|Required License|None|

|||||
|---|---|---|---|
|1st|150,000 cr|4th|10,000 cr|
|2nd|75,000 cr|5th|5,000 cr|
|3rd|20,000 cr|6th|2,000 cr|

|||
|---|---|
|Winning Prize|300,000 cr|

- [ ] 1963 Alfa Romeo Giulia Sprint Speciale

  - [ ] `[ | | | | | | | ]` 25 laps - Nurburgring Nordschleife
  - [ ] `[ | | | | | | | ]` 95 laps - Opera Paris
  - [ ] `[ | | | | | | | ]` 99 laps - Cote d' Azur
  - [ ] `[ | | | | | | | ]` 35 laps - Circuit de la Sarthe II

### 10.10 Schwarzwald Liga A

|||
|---|---|
|Game Mode|Single Race|
|Tires|Standard or Sports Tires required|
|Country Restrictions|German cars only|
|Cars Permitted|BMW 330i|
||Audi A3 3.2 quattro|
||VW Golf GTI|
||Opel Speedster Turbo|
||Mercedes SLK 230 Kompressor|
||etc.|
|Required License|A-Spec: National A License|

|||||
|---|---|---|---|
|1st|5,000 cr|4th|900 cr|
|2nd|2,000 cr|5th|800 cr|
|3rd|1,000 cr|6th|500 cr|

- [ ] 2001 BMW M3 GTR Race Car

  - [ ] `[ | | | | | | | ]` 3 laps - Trial Mountain Circuit
  - [ ] `[ | | | | | | | ]` 4 laps - Opera Paris Reverse
  - [ ] `[ | | | | | | | ]` 4 laps - Autumn Ring

### 10.11 Schwarzwald Liga B

|||
|---|---|
|Game Mode|Single Race|
|Tires|Standard or Sports Tires required|
|Country Restrictions|German cars only|
|Cars Permitted|Mercedes SLR McLaren|
||BMW M3|
||Audi RS 6|
||RUF RGT|
||VW W12 Nardo Concept|
||etc.|
|Required License|A-Spec: International B License|

|||||
|---|---|---|---|
|1st|10,000 cr|4th|3,000 cr|
|2nd|6,000 cr|5th|1,000 cr|
|3rd|4,000 cr|6th|500 cr|

- [ ] 1992 Mercedes-Benz AMG Mercedes 190 E 2.5 - 16 Evolution II Touring Car

  - [ ] `[ | | | | | | | ]` 1 lap  - Nurburgring Nordschleife
  - [ ] `[ | | | | | | | ]` 2 laps - Tokyo Route 246
  - [ ] `[ | | | | | | | ]` 3 laps - Fuji Speedway 90's
  - [ ] `[ | | | | | | | ]` 3 laps - High-Speed Ring Reverse
  - [ ] `[ | | | | | | | ]` 2 laps - Suzuka Circuit

-------------------------------------------------------------------------------
## 11 Japanese Events
-------------------------------------------------------------------------------

### 11.1 Japan Championship

|||
|---|---|
|Game Mode|Championship Race|
|Tires|Standard or Sports Tires required|
|Country Restrictions|Japanese cars only|
|Cars Permitted|All cars permitted|
|Required License|A-Spec: National A License|

|||||
|---|---|---|---|
|1st|7,500 cr|4th|1,000 cr|
|2nd|5,000 cr|5th|500 cr|
|3rd|2,500 cr|6th|300 cr|

|||
|---|---|
|Winning Prize|35,000 cr|

- [ ] 2002 Nissan 350Z Concept LM Race Car

  - [ ] `[ | | | | | | | ]`  5 laps - Fuji Speedway 90's
  - [ ] `[ | | | | | | | ]` 10 laps - Tsukuba Circuit
  - [ ] `[ | | | | | | | ]`  5 laps - Tokyo Route 246 Reverse
  - [ ] `[ | | | | | | | ]`  4 laps - Twin Ring Motegi (Road Course)
  - [ ] `[ | | | | | | | ]`  4 laps - Suzuka Circuit

### 11.2 All Japan GT Championship

|||
|---|---|
|Game Mode|Championship Race|
|Tires|No restrictions|
|Cars Permitted|All cars permitted|
|Required License|A-Spec: International A License|

|||||
|---|---|---|---|
|1st|15,000 cr|4th|4,000 cr|
|2nd|7,500 cr|5th|1,500 cr|
|3rd|5,000 cr|6th|800 cr|

|||
|---|---|
|Winning Prize|100,000 cr|

- [ ] 2004 Nissan Motul Pitwork Z (JGTC)

  - [ ] `[ | | | | | | | ]`  5 laps - Tokyo Route 246
  - [ ] `[ | | | | | | | ]` 12 laps - Suzuka Circuit (East Course)
  - [ ] `[ | | | | | | | ]` 10 laps - Seoul Central
  - [ ] `[ | | | | | | | ]`  6 laps - Fuji Speedway 90's
  - [ ] `[ | | | | | | | ]` 12 laps - Twin Ring Motegi (Super Speedway)
  - [ ] `[ | | | | | | | ]`  7 laps - Mazda Raceway Laguna Seca
  - [ ] `[ | | | | | | | ]`  9 laps - Hong Kong
  - [ ] `[ | | | | | | | ]`  6 laps - Twin Ring Motegi (Road Course)
  - [ ] `[ | | | | | | | ]`  6 laps - Fuji speedway 2005 GT
  - [ ] `[ | | | | | | | ]`  5 laps - Suzuka Circuit

### 11.3 Japanese 70's Classics

|||
|---|---|
|Game Mode|Single Race|
|Car Type|Production cars only|
|Tires|Standard or Sports Tires required|
|Country Restrictions|Japanese cars only|
|Model Year Restrictions|From 1970 to 1979|
|Cars Permitted|All cars permitted|
|Required License|A-Spec: National B License|

|||||
|---|---|---|---|
|1st|2,000 cr|4th|400 cr|
|2nd|1,000 cr|5th|300 cr|
|3rd|500 cr|6th|200 cr|

- [ ] 1970 Nissan Skyline Hard Top 2000GT-R (KPGC10)

  - [ ] `[ | | | | | | | ]` 2 laps - Fuji Speedway 80's
  - [ ] `[ | | | | | | | ]` 4 laps - Autumn Ring Mini Reverse
  - [ ] `[ | | | | | | | ]` 3 laps - Tsukuba Circuit
  - [ ] `[ | | | | | | | ]` 4 laps - Motorland Reverse
  - [ ] `[ | | | | | | | ]` 2 laps - Trial Mountain Circuit

### 11.4 Japanese 80's Festival

|||
|---|---|
|Game Mode|Single Race|
|Car Type|Production cars only|
|Tires|Standard or Sports Tires required|
|Country Restrictions|Japanese cars only|
|Model Year Restrictions|From 1980 to 1989|
|Cars Permitted|All cars permitted|
|Required License|A-Spec: National A License|

|||||
|---|---|---|---|
|1st|3,000 cr|4th|400 cr|
|2nd|1,000 cr|5th|300 cr|
|3rd|500 cr|6th|200 cr|

- [ ] 1989 Mitsubishi HSR-II Concept

  - [ ] `[ | | | | | | | ]` 3 laps - El Capitan
  - [ ] `[ | | | | | | | ]` 5 laps - Clubman Stage Route 5 Reverse
  - [ ] `[ | | | | | | | ]` 5 laps - Tsukuba Circuit
  - [ ] `[ | | | | | | | ]` 3 laps - Apricot Hill Raceway Reverse
  - [ ] `[ | | | | | | | ]` 2 laps - Suzuka Circuit

### 11.5 Japanese 90's Challenge

|||
|---|---|
|Game Mode|Single Race|
|Car Type|Production cars only|
|Tires|Standard or Sports Tires required|
|Country Restrictions|Japanese cars only|
|Model Year Restrictions|From 1990 to 1999|
|Cars Permitted|All cars permitted|
|Required License|A-Spec: National A License|

|||||
|---|---|---|---|
|1st|3,000 cr|4th|400 cr|
|2nd|1,000 cr|5th|300 cr|
|3rd|500 cr|6th|200 cr|

- [ ] 1996 Nismo 400R

  - [ ] `[ | | | | | | | ]` 3 laps - Apricot Hill Raceway
  - [ ] `[ | | | | | | | ]` 4 laps - Grand Valley East Reverse
  - [ ] `[ | | | | | | | ]` 3 laps - Twin Ring Motegi (East Short Course)
  - [ ] `[ | | | | | | | ]` 3 laps - Special Stage Route 5 Reverse
  - [ ] `[ | | | | | | | ]` 3 laps - Fuji Speedway 90's

### 11.6 Japanese Compact Cup

|||
|---|---|
|Game Mode|Championship Race|
|Tires|Standard or Sports Tires required|
|Length Limit|4000 mm or less|
|Country Restrictions|Japanese cars only|
|Cars Permitted|All cars permitted|
|Required License|A-Spec: National B License|

|||||
|---|---|---|---|
|1st|2,000 cr|4th|400 cr|
|2nd|1,000 cr|5th|300 cr|
|3rd|500 cr|6th|200 cr|

|||
|---|---|
|Winning Prize|15,000 cr|

- [ ] 1972 Honda Life Step Van

  - [ ] `[ | | | | | | | ]` 2 laps - Twin Ring Motegi (East Short Course)
  - [ ] `[ | | | | | | | ]` 4 laps - Autumn Ring Mini Reverse
  - [ ] `[ | | | | | | | ]` 3 laps - Tsukuba Circuit
  - [ ] `[ | | | | | | | ]` 2 laps - Deep Forest Raceway Reverse
  - [ ] `[ | | | | | | | ]` 3 laps - Suzuka Circuit (East Course)

-------------------------------------------------------------------------------
## 12 One-make Races: France
-------------------------------------------------------------------------------

### 12.1 Alpine: Renault Alpine Cup

|||
|---|---|
|Game Mode|Championship Race|
|Tires|No restrictions|
|Cars Permitted|Alpine A310 1600VE|
||Alpine A110 1600S|
|Required License|A-Spec: National A License|

|||||
|---|---|---|---|
|1st|7,500 cr|4th|1,000 cr|
|2nd|5,000 cr|5th|500 cr|
|3rd|2,500 cr|6th|300 cr|

|||
|---|---|
|Winning Prize|50,000 cr|

- [ ] 1973 Alpine A110 1600S

  - [ ] `[ | | | | | | | ]` 2 laps - Tokyo Route 246
  - [ ] `[ | | | | | | | ]` 3 laps - Trial Mountain Circuit
  - [ ] `[ | | | | | | | ]` 3 laps - Opera Paris Reverse
  - [ ] `[ | | | | | | | ]` 2 laps - Special Stage Route 5
  - [ ] `[ | | | | | | | ]` 2 laps - Grand Valley Speedway

### 12.2 Citroen: 2HP - 2CV Classics

|||
|---|---|
|Game Mode|Championship Race|
|Tires|No restrictions|
|Cars Permitted|Citroen 2CV Type A|
|Required License|None|

|||||
|---|---|---|---|
|1st|2,000 cr|4th|300 cr|
|2nd|500 cr|5th|200 cr|
|3rd|400 cr|6th|100 cr|

|||
|---|---|
|Winning Prize|10,000 cr|

- [ ] 1954 Citroen 2CV Type A (special color)

  - [ ] `[ | | | | | | | ]` 1 lap  - Opera Paris Reverse
  - [ ] `[ | | | | | | | ]` 1 lap  - Cote d' Azur
  - [ ] `[ | | | | | | | ]` 2 laps - Autumn Ring Mini
  - [ ] `[ | | | | | | | ]` 1 lap  - Deep Forest Raceway Reverse
  - [ ] `[ | | | | | | | ]` 2 laps - Tsukuba Circuit

### 12.3 Peugeot: 206 Cup

|||
|---|---|
|Game Mode|Single Race|
|Tires|No restrictions|
|Cars Permitted|Peugeot 206 RC|
||Peugeot 206cc|
||Peugeot 206 S16|
||Peugeot 206 Rally Car|
|Required License|A-Spec: International B License|

|||||
|---|---|---|---|
|1st|7,500 cr|4th|1,000 cr|
|2nd|5,000 cr|5th|500 cr|
|3rd|2,500 cr|6th|300 cr|

- [ ] 1985 Peugeot 205 Turbo 16 Rally Car

  - [ ] `[ | | | | | | | ]` 4 laps - Opera Paris
  - [ ] `[ | | | | | | | ]` 5 laps - Tsukuba Circuit
  - [ ] `[ | | | | | | | ]` 8 laps - Motorland Reverse
  - [ ] `[ | | | | | | | ]` 5 laps - Suzuka Circuit (East Course)
  - [ ] `[ | | | | | | | ]` 3 laps - Cote d' Azur

### 12.4 Renault: Clio Trophy

|||
|---|---|
|Game Mode|Championship Race|
|Tires|No restrictions|
|Cars Permitted|Clio Renault Sport V6 Phase 2|
||Clio Renault Sport 2.0 16V|
||Clio Renault Sport V6 24V|
||Clio Sport Trophy V6 24V Race Car|
|Required License|A-Spec: International B License|

|||||
|---|---|---|---|
|1st|10,000 cr|4th|3,000 cr|
|2nd|6,000 cr|5th|1,000 cr|
|3rd|4,000 cr|6th|500 cr|

|||
|---|---|
|Winning Prize|50,000 cr|

- [ ] 2000 Renault Clio Renault Sport Trophy V6 24V Race Car

  - [ ] `[ | | | | | | | ]` 3 laps - Suzuka Circuit
  - [ ] `[ | | | | | | | ]` 4 laps - Twin Ring Motegi (Road Course)
  - [ ] `[ | | | | | | | ]` 7 laps - Tsukuba Circuit
  - [ ] `[ | | | | | | | ]` 4 laps - Deep Forest Raceway
  - [ ] `[ | | | | | | | ]` 4 laps - El Capitan Reverse

### 12.5 Renault: Megane Cup

|||
|---|---|
|Game Mode|Championship Race|
|Tires|No restrictions|
|Cars Permitted|Renault Megane 2.0 16V|
||Renault Megane 2.0 IDE Coupe|
|Required License|A-Spec: International B License|

|||||
|---|---|---|---|
|1st|7,500 cr|4th|1,000 cr|
|2nd|5,000 cr|5th|500 cr|
|3rd|2,500 cr|6th|300 cr|

|||
|---|---|
|Winning Prize|35,000 cr|

- [ ] 2002 Renault Avantime

  - [ ] `[ | | | | | | | ]` 3 laps - Suzuka Circuit (East Course)
  - [ ] `[ | | | | | | | ]` 3 laps - Opera Paris
  - [ ] `[ | | | | | | | ]` 2 laps - Mazda Raceway Laguna Seca
  - [ ] `[ | | | | | | | ]` 3 laps - Seoul Central
  - [ ] `[ | | | | | | | ]` 2 laps - Cote d' Azur

-------------------------------------------------------------------------------
## 13 One-make Races: Germany
-------------------------------------------------------------------------------

### 13.1 Audi: Tourist Trophy

|||
|---|---|
|Game Mode|Championship Race|
|Tires|No restrictions|
|Cars Permitted|Audi TT Coupe 3.2 quattro|
||Abt Audi TT-R Touring Car|
||Audi TT Coupe 1.8T quattro|
|Required License|A-Spec: National B License|

|||||
|---|---|---|---|
|1st|5,000 cr|4th|900 cr|
|2nd|2,000 cr|5th|800 cr|
|3rd|1,000 cr|6th|500 cr|

|||
|---|---|
|Winning Prize|20,000 cr|

- [ ] 2003 Audi Le Mans quattro

  - [ ] `[ | | | | | | | ]` 2 laps - El Capitan
  - [ ] `[ | | | | | | | ]` 1 lap  - Tokyo Route 246 Reverse
  - [ ] `[ | | | | | | | ]` 2 laps - Deep Forest Raceway Reverse

### 13.2 Audi: A3 Cup

|||
|---|---|
|Game Mode|Single Race|
|Tires|No restrictions|
|Cars Permitted|Audi A3 3.2 quattro|
|Required License|None|

|||||
|---|---|---|---|
|1st|2,000 cr|4th|300 cr|
|2nd|500 cr|5th|200 cr|
|3rd|400 cr|6th|100 cr|

- [ ] 2003 Audi Pikes Peak quattro

  - [ ] `[ | | | | | | | ]` 3 laps - Suzuka Circuit (East Course)
  - [ ] `[ | | | | | | | ]` 2 laps - Mid-Field Raceway
  - [ ] `[ | | | | | | | ]` 2 laps - Mazda Raceway Laguna Seca

### 13.3 BMW: 1 Series Trophy

|||
|---|---|
|Game Mode|Single Race|
|Tires|No restrictions|
|Cars Permitted|BMW 120i|
||BMW 120d|
|Required License|None|

|||||
|---|---|---|---|
|1st|3,000 cr|4th|400 cr|
|2nd|1,000 cr|5th|300 cr|
|3rd|500 cr|6th|200 cr|

- [ ] 1973 BMW 2002 Turbo

  - [ ] `[ | | | | | | | ]` 1 lap  - Grand Valley Speedway
  - [ ] `[ | | | | | | | ]` 2 laps - Infineon Raceway (Sports Car Course)
  - [ ] `[ | | | | | | | ]` 2 laps - Twin Ring Motegi (East Short Course)

### 13.4 BMW: Club "M"

|||
|---|---|
|Game Mode|Championship Race|
|Tires|No restrictions|
|Cars Permitted|BMW M5|
||BMW M3|
||BMW M3 CSL|
||BMW M3 GTR|
||BMW M Coupe|
||etc.|
|Required License|A-spec National A License|

|||||
|---|---|---|---|
|1st|10,000 cr|4th|3,000 cr|
|2nd|6,000 cr|5th|1,000 cr|
|3rd|4,000 cr|6th|500 cr|

|||
|---|---|
|Winning Prize|35,000 cr|

- [ ] 2003 BMW M3 GTR

  - [ ] `[ | | | | | | | ]` 2 laps - El Capitan Reverse
  - [ ] `[ | | | | | | | ]` 3 laps - Apricot Hill Raceway
  - [ ] `[ | | | | | | | ]` 4 laps - Opera Paris Reverse
  - [ ] `[ | | | | | | | ]` 2 laps - Suzuka Circuit
  - [ ] `[ | | | | | | | ]` 1 lap  - Nurburgring Nordschleife

### 13.5 Mercedes Benz: Legends of the Silver Arrow

|||
|---|---|
|Game Mode|Single Race|
|Tires|No restrictions|
|Cars Permitted|Mercedes SL 600 (R230)|
||Mercedes SLR McLaren|
||Mercedes SL 500 (R230)|
||Mercedes E 55 AMG|
||Mercedes CLK 55 AMG|
||etc.|
|Required License|A-Spec: National A License|

|||||
|---|---|---|---|
|1st|7,500 cr|4th|1,000 cr|
|2nd|5,000 cr|5th|500 cr|
|3rd|2,500 cr|6th|300 cr|

- [ ] 2000 Mercedes-Benz CLK Touring Car

  - [ ] `[ | | | | | | | ]` 3 laps - Fuji Speedway 90's
  - [ ] `[ | | | | | | | ]` 1 lap  - Nurburgring Nordschleife
  - [ ] `[ | | | | | | | ]` 3 laps - Opera Paris

### 13.6 Mercedes Benz: SL Challenge

|||
|---|---|
|Game Mode|Single Race|
|Tires|No restrictions|
|Cars Permitted|Mercedes SL 65 AMG (R230)|
||Mercedes SL 600 (R230)|
||Mercedes SL 55 AMG (R230)|
||Mercedes SL 500 (R230)|
||Mercedes 300 SL Coupe|
||etc.|
|Required License|A-Spec: International B License|

|||||
|---|---|---|---|
|1st|10,000 cr|4th|3,000 cr|
|2nd|6,000 cr|5th|1,000 cr|
|3rd|4,000 cr|6th|500 cr|

- [ ] 1954 Mercedes-Benz 300 SL Coupe

  - [ ] `[ | | | | | | | ]` 4 laps - Apricot Hill Raceway
  - [ ] `[ | | | | | | | ]` 4 laps - Fuji Speedway 80's
  - [ ] `[ | | | | | | | ]` 4 laps - El Capitan

### 13.7 Opel: Speedster Trophy

|||
|---|---|
|Game Mode|Single Race|
|Tires|No restrictions|
|Cars Permitted|Opel Speedster Turbo|
||Opel Speedster|
|Required License|A-Spec International B License|

|||||
|---|---|---|---|
|1st|7,500 cr|4th|1,000 cr|
|2nd|5,000 cr|5th|500 cr|
|3rd|2,500 cr|6th|300 cr|

- [ ] 1994 Opel Calibra Touring Car (DTM)

  - [ ] `[ | | | | | | | ]` 3 laps - Grand Valley Speedway
  - [ ] `[ | | | | | | | ]` 4 laps - Autumn Ring Reverse
  - [ ] `[ | | | | | | | ]` 4 laps - Suzuka Circuit (East Course)
  - [ ] `[ | | | | | | | ]` 3 laps - Infineon Raceway (Sports Car Course)
  - [ ] `[ | | | | | | | ]` 3 laps - Seattle Circuit

### 13.8 Volkswagen: Beetle Cup

|||
|---|---|
|Game Mode|Championship Race|
|Tires|No restrictions|
|Cars Permitted|VW New Beetle 2.0|
||VW New Beetle RSi|
||VW New Beetle Cup Car|
||VW Beetle 1100 Standard (Type-11)|
|Required License|A-Spec: National B License|

|||||
|---|---|---|---|
|1st|2,500 cr|4th|400 cr|
|2nd|1,000 cr|5th|300 cr|
|3rd|500 cr|6th|200 cr|

|||
|---|---|
|Winning Prize|25,000 cr|

- [ ] 1949 Volkswagen Beetle 1100 Standard (Type-11)

  - [ ] `[ | | | | | | | ]` 2 laps - Fuji Speedway 90's
  - [ ] `[ | | | | | | | ]` 2 laps - Twin Ring Motegi (Road Course)
  - [ ] `[ | | | | | | | ]` 3 laps - Tsukuba Circuit
  - [ ] `[ | | | | | | | ]` 1 lap  - Suzuka Circuit
  - [ ] `[ | | | | | | | ]` 2 laps - Fuji Speedway 2005

### 13.9 Volkswagen: Lupo Cup

|||
|---|---|
|Game Mode|Championship Race|
|Tires|No restrictions|
|Cars Permitted|VW Lupo GTI Cup Car (J)|
||VW Lupo 1.4|
||VW Lupo GTI|
||VW Lupo Cup Car|
|Required License|A-Spec: National A License|

|||||
|---|---|---|---|
|1st|5,000 cr|4th|400 cr|
|2nd|1,000 cr|5th|300 cr|
|3rd|500 cr|6th|200 cr|

|||
|---|---|
|Winning Prize|25,000 cr|

- [ ] 1968 Volkswagen Karmann Ghia Coupe (Type-1)

  - [ ] `[ | | | | | | | ]` 2 laps - Fuji Speedway 90's
  - [ ] `[ | | | | | | | ]` 1 lap  - Nurburgring Nordschleife
  - [ ] `[ | | | | | | | ]` 3 laps - Tsukuba Circuit
  - [ ] `[ | | | | | | | ]` 1 lap  - Suzuka Circuit
  - [ ] `[ | | | | | | | ]` 2 laps - Infineon Raceway (Sports Car Course)

### 13.10 Volkswagen: GTi Cup

|||
|---|---|
|Game Mode|Single Race|
|Tires|No restrictions|
|Cars Permitted|VW Golf V GTI|
||VW Lupo GTI|
||VW Polo GTI|
||VW Golf IV GTI|
||VW Golf I GTI|
||etc.|
|Required License|A-Spec: National B License|

|||||
|---|---|---|---|
|1st|2,000 cr|4th|400 cr|
|2nd|1,000 cr|5th|300 cr|
|3rd|500 cr|6th|200 cr|

- [ ] 1976 Volkswagen Golf I GTI

  - [ ] `[ | | | | | | | ]` 2 laps - Hong Kong Reverse
  - [ ] `[ | | | | | | | ]` 3 laps - Tsukuba Circuit
  - [ ] `[ | | | | | | | ]` 2 laps - Opera Paris
  - [ ] `[ | | | | | | | ]` 2 laps - Mid-Field Raceway Reverse
  - [ ] `[ | | | | | | | ]` 2 laps - Deep Forest Raceway Reverse

-------------------------------------------------------------------------------
## 14 One-make Races: Italy
-------------------------------------------------------------------------------

### 14.1 Alfa Romeo: GTA Cup

|||
|---|---|
|Game Mode|Single Race|
|Tires|No restrictions|
|Cars Permitted|Alfa Romeo 147 GTA|
||Alfa Romeo Giulia Sprint GTA 1600|
|Required License|None|

|||||
|---|---|---|---|
|1st|2,000 cr|4th|300 cr|
|2nd|500 cr|5th|200 cr|
|3rd|400 cr|6th|100 cr|

- [ ] 1965 Alfa Romeo Giulia Sprint GTA 1600

  - [ ] `[ | | | | | | | ]` 2 laps - Grand Valley East Reverse
  - [ ] `[ | | | | | | | ]` 3 laps - Tsukuba Circuit
  - [ ] `[ | | | | | | | ]` 2 laps - Apricot Hill Raceway

-------------------------------------------------------------------------------
## 15 One-make Races: Japan
-------------------------------------------------------------------------------

### 15.1 Daihatsu: Copen Race

|||
|---|---|
|Game Mode|Single Race|
|Tires|No restrictions|
|Cars Permitted|Daihatsu Copen Active Top|
||Daihatsu Copen Detachable Top|
|Required License|A-Spec: National A License|

|||||
|---|---|---|---|
|1st|7,500 cr|4th|1,000 cr|
|2nd|5,000 cr|5th|500 cr|
|3rd|2,500 cr|6th|300 cr|

- [ ] 2000 Daihatsu Storia X4

  - [ ] `[ | | | | | | | ]` 2 laps - Tsukuba Circuit
  - [ ] `[ | | | | | | | ]` 3 laps - Motorland
  - [ ] `[ | | | | | | | ]` 3 laps - Autumn Ring Mini

### 15.2 Daihatsu: Midget II Race

|||
|---|---|
|Game Mode|Single Race|
|Tires|No restrictions|
|Cars Permitted|Daihatsu Midget II D-type|
|Required License|None|

|||||
|---|---|---|---|
|1st|2,000 cr|4th|300 cr|
|2nd|500 cr|5th|200 cr|
|3rd|400 cr|6th|100 cr|

- [ ] 1963 Daihatsu Midget

  - [ ] `[ | | | | | | | ]` 4 laps - Motorland Reverse

### 15.3 Honda: Type R Meeting

|||
|---|---|
|Game Mode|Single Race|
|Tires|No restrictions|
|Cars Permitted|Honda CIVIC TYPE R (EP)|
||Honda INTEGRA TYPE R (DC5)|
||Honda NSX Type R|
||Honda ACCORD Euro-R|
||Honda INTEGRA TYPE R (DC2)|
||etc.|
|Required License|A-Spec: International B License|

|||||
|---|---|---|---|
|1st|10,000 cr|4th|3,000 cr|
|2nd|6,000 cr|5th|1,000 cr|
|3rd|4,000 cr|6th|500 cr|

- [ ] 2004 Acura HSC

  - [ ] `[ | | | | | | | ]`  4 laps - Twin Ring Motegi (Road Course)
  - [ ] `[ | | | | | | | ]`  5 laps - Autumn Ring Reverse
  - [ ] `[ | | | | | | | ]`  4 laps - Suzuka Circuit
  - [ ] `[ | | | | | | | ]` 10 laps - Motorland Reverse
  - [ ] `[ | | | | | | | ]` 10 laps - Tsukuba Circuit

### 15.4 Honda: Civic Race

|||
|---|---|
|Game Mode|Single Race|
|Tires|No restrictions|
|Cars Permitted|Honda CIVIC TYPE R (EP)|
||Honda CIVIC TYPE R (EK)|
||Honda CIVIC SiR-II (EG)|
||Honda CIVIC 1500 3door 25i|
||Honda CIVIC 1500 3door CX|
||etc.|
|Required License|A-Spec: National B License|

|||||
|---|---|---|---|
|1st|5,000 cr|4th|900 cr|
|2nd|2,000 cr|5th|800 cr|
|3rd|1,000 cr|6th|500 cr|

- [ ] 1987 Honda Mugen Motul Civic Si Race Car

  - [ ] `[ | | | | | | | ]` 2 laps - Twin Ring Motegi (East Short Course)
  - [ ] `[ | | | | | | | ]` 3 laps - Suzuka Circuit (East Course)
  - [ ] `[ | | | | | | | ]` 2 laps - Deep Forest Raceway
  - [ ] `[ | | | | | | | ]` 3 laps - Autumn Ring Mini
  - [ ] `[ | | | | | | | ]` 2 laps - Mid-Field Raceway Reverse

### 15.5 Isuzu: Isuzu Sports Classics

|||
|---|---|
|Game Mode|Single Race|
|Tires|No restrictions|
|Cars Permitted|Isuzu PIAZZA XE|
||Isuzu Bellett 1600 GT-R|
||Isuzu 117 Coupe|
|Required License|None|

|||||
|---|---|---|---|
|1st|2,000 cr|4th|300 cr|
|2nd|500 cr|5th|200 cr|
|3rd|400 cr|6th|100 cr|

- [ ] 1968 Isuzu 117 Coupe

  - [ ] `[ | | | | | | | ]` 3 laps - Tsukuba Circuit
  - [ ] `[ | | | | | | | ]` 2 laps - Fuji Speedway 80's
  - [ ] `[ | | | | | | | ]` 2 laps - Trial Mountain Circuit

### 15.6 Mazda: Club "RE"

|||
|---|---|
|Game Mode|Single Race|
|Tires|No restrictions|
|Cars Permitted|Mazda RX-7 Spirit R Type A (FD)|
||Mazda RX-8 Type S (J)|
||Mazda RX-7 Type R (FD, J)|
||Mazda RX-7 GT-X (FC, J)|
||Mazda 110S (L10B)|
||etc.|
|Required License|A-Spec: National B License|

|||||
|---|---|---|---|
|1st|5,000 cr|4th|900 cr|
|2nd|2,000 cr|5th|800 cr|
|3rd|1,000 cr|6th|500 cr|

- [ ] 1967 Mazda 110S (L10A)

  - [ ] `[ | | | | | | | ]` 2 laps - Mazda Raceway Laguna Seca
  - [ ] `[ | | | | | | | ]` 2 laps - Autumn Ring Reverse
  - [ ] `[ | | | | | | | ]` 2 laps - Special Stage Route 5 Reverse
  - [ ] `[ | | | | | | | ]` 2 laps - Opera Paris
  - [ ] `[ | | | | | | | ]` 1 lap  - Suzuka Circuit

### 15.7 Mazda: NR-A Roadster Cup

|||
|---|---|
|Game Mode|Championship Race|
|Tires|No restrictions|
|Cars Permitted|Mazda MX-5 Miata 1800 RS (J)|
||Mazda MX-5 Miata 1600 NR-A (J)|
||Mazda MX-5 Miata 1800 RS (J)|
||Mazda MX-5 Miata 1.8 RS (J)|
||Mazda MX-5 Miata VR-Limited (J)|
||etc.|
|Required License|A-Spec: National B License|

|||||
|---|---|---|---|
|1st|2,000 cr|4th|400 cr|
|2nd|1,000 cr|5th|300 cr|
|3rd|500 cr|6th|200 cr|

|||
|---|---|
|Winning Prize|15,000 cr|

- [ ] 2005 Mazda MX-Crossport

  - [ ] `[ | | | | | | | ]` 2 laps - Twin Ring Motegi (East Short Course)
  - [ ] `[ | | | | | | | ]` 1 lap  - Tokyo Route 246
  - [ ] `[ | | | | | | | ]` 2 laps - Clubman Stage Route 5 Reverse
  - [ ] `[ | | | | | | | ]` 3 laps - Tsukuba Circuit
  - [ ] `[ | | | | | | | ]` 2 laps - Trial Mountain Circuit

### 15.8 Mazda: NR-A RX-8 Cup

|||
|---|---|
|Game Mode|Championship Race|
|Tires|No restrictions|
|Cars Permitted|Mazda RX-8 Type E (J)|
||Mazda RX-8 Type S (J)|
||Mazda RX-8|
||Mazda RX-8 Concept (Type-I)|
||Mazda RX-8 Concept (Type-II)|
||etc.|
|Required License|A-Spec: National B License|

|||||
|---|---|---|---|
|1st|2,000 cr|4th|400 cr|
|2nd|1,000 cr|5th|300 cr|
|3rd|500 cr|6th|200 cr|

|||
|---|---|
|Winning Prize|15,000 cr|

- [ ] 2001 Mazda RX-8 Concept LM Race Car

  - [ ] `[ | | | | | | | ]` 2 laps - Mazda Raceway Laguna Seca
  - [ ] `[ | | | | | | | ]` 2 laps - Special Stage Route 5
  - [ ] `[ | | | | | | | ]` 2 laps - Fuji Speedway 90's
  - [ ] `[ | | | | | | | ]` 2 laps - El Capitan
  - [ ] `[ | | | | | | | ]` 3 laps - Tsukuba Circuit

### 15.9 Mitsubishi: Evolution Meeting

|||
|---|---|
|Game Mode|Single Race|
|Tires|No restrictions|
|Cars Permitted|Mitsubishi Lancer Evolution VIII MR GSR|
||Mitsubishi Lancer Evolution VIII GSR|
||Mitsubishi Lancer Evolution VII GSR|
||Mitsubishi Lancer Evolution VI GSR|
||Mitsubishi Lancer Evolution V GSR|
||etc.|
|Required License|A-Spec: National B License|

|||||
|---|---|---|---|
|1st|2,000 cr|4th|400 cr|
|2nd|1,000 cr|5th|300 cr|
|3rd|500 cr|6th|200 cr|

- [ ] 1974 Mitsubishi Lancer 1600 GSR Rally Car

  - [ ] `[ | | | | | | | ]` 3 laps - Clubman Stage Route 5
  - [ ] `[ | | | | | | | ]` 2 laps - Fuji Speedway 90's
  - [ ] `[ | | | | | | | ]` 1 lap  - Tokyo Route 246

### 15.10 Mitsubishi: Mirage Cup

|||
|---|---|
|Game Mode|Championship Race|
|Tires|No restrictions|
|Cars Permitted|Mitsubishi MIRAGE CYBORG ZR|
||Mitsubishi MIRAGE 1400GLX|
|Required License|None|

|||||
|---|---|---|---|
|1st|2,000 cr|4th|300 cr|
|2nd|500 cr|5th|200 cr|
|3rd|400 cr|6th|100 cr|

|||
|---|---|
|Winning Prize|15,000 cr|

- [ ] 2003 Mitsubishi i

  - [ ] `[ | | | | | | | ]` 3 laps - Tsukuba Circuit
  - [ ] `[ | | | | | | | ]` 2 laps - Autumn Ring
  - [ ] `[ | | | | | | | ]` 2 laps - Special Stage Route 5
  - [ ] `[ | | | | | | | ]` 2 laps - Mazda Raceway Laguna Seca
  - [ ] `[ | | | | | | | ]` 1 lap  - Suzuka Circuit

### 15.11 Nissan: Race of the Red "R" Emblem

|||
|---|---|
|Game Mode|Single Race|
|Tires|No restrictions|
|Cars Permitted|Nissan SKYLINE Sport Coupe (BLRA-3)|
||Nissan SKYLINE 1500Deluxe (S50D-1)|
||Nissan SKYLINE 2000GT-B (S54A)|
||Nissan SKYLINE HT 2000GT-R (KPGC10)|
||Nissan SKYLINE 2000GT-R (KPGC110)|
||etc.|
|Required License|A-Spec: International A License|

|||||
|---|---|---|---|
|1st|12,000 cr|4th|4,000 cr|
|2nd|6,500 cr|5th|1,500 cr|
|3rd|5,000 cr|6th|800 cr|

- [ ] 1995 Nismo GT-R LM Road Going Version

  - [ ] `[ | | | | | | | ]` 10 laps - Twin Ring Motegi (Road Course)
  - [ ] `[ | | | | | | | ]`  6 laps - Trial Mountain Reverse
  - [ ] `[ | | | | | | | ]`  4 laps - Fuji Speedway 80's
  - [ ] `[ | | | | | | | ]`  6 laps - New York Reverse
  - [ ] `[ | | | | | | | ]`  6 laps - Suzuka Circuit

### 15.12 Nissan: March Brothers

|||
|---|---|
|Game Mode|Single Race|
|Tires|No restrictions|
|Cars Permitted|Nissan MICRA|
||Nissan CUBE EX (FF/CVT)|
||Nissan March G#|
||Nissan PAO|
||Nissan Be-1|
||etc.|
|Required License|A-Spec: National B License|

|||||
|---|---|---|---|
|1st|2,000 cr|4th|400 cr|
|2nd|1,000 cr|5th|300 cr|
|3rd|500 cr|6th|200 cr|

- [ ] 2001 Nissan mm-R Cup Car

  - [ ] `[ | | | | | | | ]` 4 laps - Autumn Ring Mini
  - [ ] `[ | | | | | | | ]` 3 laps - Suzuka Circuit (East Course)
  - [ ] `[ | | | | | | | ]` 2 laps - Grand Valley East Reverse

### 15.13 Nissan: Silvia Sisters

|||
|---|---|
|Game Mode|Single Race|
|Tires|No restrictions|
|Cars Permitted|Nissan SILVIA Spec R AERO (S15)|
||Nissan 240SX (S14)|
||Nissan SILVIA K's (S13)|
||Nissan 240SX|
||Nissan SILVIA Varietta (S15)|
||etc.|
|Required License|A-Spec: National B License|

|||||
|---|---|---|---|
|1st|2,000 cr|4th|400 cr|
|2nd|1,000 cr|5th|300 cr|
|3rd|500 cr|6th|200 cr|

- [ ] 1998 Nissan Sileighty

  - [ ] `[ | | | | | | | ]` 3 laps - Tsukuba Circuit
  - [ ] `[ | | | | | | | ]` 2 laps - Special Stage Route 5
  - [ ] `[ | | | | | | | ]` 2 laps - Fuji Speedway 90's

### 15.14 Nissan: Club "Z"

|||
|---|---|
|Game Mode|Single Race|
|Tires|No restrictions|
|Cars Permitted|Nissan Fairlady Z Version ST (Z33)|
||Nissan 350Z Roadster (Z33)|
||Nissan 300ZX 2seater (Z32)|
||Nissan Fairlady Z 300ZX (Z31)|
||Nissan 240ZG (HS30)|
||etc.|
|Required License|A-Spec: National A License|

|||||
|---|---|---|---|
|1st|7,500 cr|4th|1,000 cr|
|2nd|5,000 cr|5th|500 cr|
|3rd|2,500 cr|6th|300 cr|

- [ ] 1971 Nissan 240ZG (HS30)

  - [ ] `[ | | | | | | | ]` 3 laps - New York
  - [ ] `[ | | | | | | | ]` 2 laps - Tokyo Route 246 Reverse
  - [ ] `[ | | | | | | | ]` 3 laps - El Capitan
  - [ ] `[ | | | | | | | ]` 3 laps - Mazda Raceway Laguna Seca
  - [ ] `[ | | | | | | | ]` 2 laps - Grand Valley Speedway Reverse

### 15.15 Subaru: Subaru 360 Race

|||
|---|---|
|Game Mode|Single Race|
|Tires|No restrictions|
|Cars Permitted|Subaru SUBARU 360|
|Required License|A-Spec: National A License|

|||||
|---|---|---|---|
|1st|3,000 cr|4th|400 cr|
|2nd|1,000 cr|5th|300 cr|
|3rd|500 cr|6th|200 cr|

- [ ] 1958 Subaru 360

  - [ ] `[ | | | | | | | ]` 3 laps - Tsukuba Circuit

### 15.16 Subaru: Stars of Pleiades

|||
|---|---|
|Game Mode|Championship Race|
|Tires|No restrictions|
|Cars Permitted|Subaru IMPREZA Sedan WRX STi sp.C|
||Subaru IMPREZA Sedan WRX STi|
||Subaru IMPREZA Wagon STi VecVI|
||Subaru LEGACY Wagon 3.0R|
||Subaru LEGACY B4 2.0GT specB|
||etc.|
|Required License|A-Spec: International B License|

|||||
|---|---|---|---|
|1st|10,000 cr|4th|3,000 cr|
|2nd|6,000 cr|5th|1,000 cr|
|3rd|4,000 cr|6th|500 cr|

|||
|---|---|
|Winning Prize|75,000 cr|

- [ ] 2001 Subaru Impreza Super Touring Car

  - [ ] `[ | | | | | | | ]` 5 laps - Tsukuba Circuit
  - [ ] `[ | | | | | | | ]` 3 laps - Trial Mountain Circuit
  - [ ] `[ | | | | | | | ]` 2 laps - Suzuka Circuit
  - [ ] `[ | | | | | | | ]` 3 laps - Deep Forest Raceway Reverse
  - [ ] `[ | | | | | | | ]` 3 laps - Mazda Raceway Laguna Seca

### 15.17 Suzuki: Suzuki K-Car Cup

|||
|---|---|
|Game Mode|Single Race|
|Tires|No restrictions|
|Cars Permitted|Suzuki MR Wagon Sport|
||Suzuki ALTO LAPIN Turbo|
||Suzuki WAGON R RR|
||Suzuki ALTO WORKS SUZUKI SPORT Ltd.|
||Suzuki Cappuccino (EA21R)|
||etc.|
|Required License|A-Spec: National B License|

|||||
|---|---|---|---|
|1st|5,000 cr|4th|900 cr|
|2nd|2,000 cr|5th|800 cr|
|3rd|1,000 cr|6th|500 cr|

- [ ] 2003 Suzuki Concept-S2

  - [ ] `[ | | | | | | | ]` 4 laps - Autumn Ring Mini Reverse
  - [ ] `[ | | | | | | | ]` 3 laps - Tsukuba Circuit
  - [ ] `[ | | | | | | | ]` 4 laps - Motorland

### 15.18 Suzuki: Suzuki Concepts

|||
|---|---|
|Game Mode|Single Race|
|Tires|No restrictions|
|Cars Permitted|Suzuki CONCEPT-S2|
||Suzuki GSX-R/4|
|Required License|None|

|||||
|---|---|---|---|
|1st|2,000 cr|4th|300 cr|
|2nd|500 cr|5th|200 cr|
|3rd|400 cr|6th|100 cr|

- [ ] 2001 Suzuki GSX-R/4

  - [ ] `[ | | | | | | | ]` 4 laps - Twin Ring Motegi (West Short Course)
  - [ ] `[ | | | | | | | ]` 2 laps - Seattle Circuit
  - [ ] `[ | | | | | | | ]` 3 laps - Tsukuba Circuit

### 15.19 Toyota: Altezza Race

|||
|---|---|
|Game Mode|Single Race|
|Tires|No restrictions|
|Cars Permitted|Lexus IS200 (J)|
||Lexus IS200|
||Lexus IS300 Sport Cross|
||Toyota ALTEZZA Touring Car|
|Required License|A-Spec: National B License|

|||||
|---|---|---|---|
|1st|2,000 cr|4th|400 cr|
|2nd|1,000 cr|5th|300 cr|
|3rd|500 cr|6th|200 cr|

- [ ] 2001 Toyota Altezza Touring Car

  - [ ] `[ | | | | | | | ]` 3 laps - Tsukuba Circuit
  - [ ] `[ | | | | | | | ]` 1 lap  - Suzuka Circuit
  - [ ] `[ | | | | | | | ]` 2 laps - Apricot Hill Raceway
  - [ ] `[ | | | | | | | ]` 2 laps - Twin Ring Motegi (Road Course)
  - [ ] `[ | | | | | | | ]` 2 laps - Fuji Speedway 2005

### 15.20 Toyota: Vitz Race

|||
|---|---|
|Game Mode|Single Race|
|Tires|No restrictions|
|Cars Permitted|Toyota VITZ RS Turbo|
||Toyota VITZ RS 1.5|
||Toyota VITZ U Euro Sport Edition|
||Toyota VITZ F|
|Required License|None|

|||||
|---|---|---|---|
|1st|2,000 cr|4th|300 cr|
|2nd|500 cr|5th|200 cr|
|3rd|400 cr|6th|100 cr|

- [ ] 2002 Toyota Vitz RS Turbo

  - [ ] `[ | | | | | | | ]` 2 laps - Fuji Speedway 90's
  - [ ] `[ | | | | | | | ]` 3 laps - Suzuka Circuit (East Course)
  - [ ] `[ | | | | | | | ]` 2 laps - Twin Ring Motegi (East Short Course)
  - [ ] `[ | | | | | | | ]` 3 laps - Tsukuba Circuit
  - [ ] `[ | | | | | | | ]` 2 laps - Autumn Ring

-------------------------------------------------------------------------------
## 16 One-make Races: Korea
-------------------------------------------------------------------------------

### 16.1 Hyundai: Hyundai Sports Festival

|||
|---|---|
|Game Mode|Single Race|
|Tires|No restrictions|
|Cars Permitted|Hyundai Tiburon GT|
||Hyundai HCD6|
||Hyundai Clix|
||Hyundai Accent Rally Car|
|Required License|A-Spec: National B License|

|||||
|---|---|---|---|
|1st|5,000 cr|4th|900 cr|
|2nd|2,000 cr|5th|800 cr|
|3rd|1,000 cr|6th|500 cr|

- [ ] 2001 Hyundai Clix

  - [ ] `[ | | | | | | | ]` 2 laps - Autumn Ring
  - [ ] `[ | | | | | | | ]` 2 laps - Hong Kong
  - [ ] `[ | | | | | | | ]` 2 laps - Grand Valley East
  - [ ] `[ | | | | | | | ]` 3 laps - Tsukuba Circuit
  - [ ] `[ | | | | | | | ]` 2 laps - Seoul Central

-------------------------------------------------------------------------------
## 17 One-make Races: UK
-------------------------------------------------------------------------------

### 17.1 Aston Martin: Aston Martin Carnival

|||
|---|---|
|Game Mode|Single Race|
|Tires|Standard or Sports Tires required|
|Cars Permitted|Aston Martin Vanquish|
||Aston Martin DB9 Coupe|
||Aston Martin DB7 Vantage Coupe|
||Aston Martin V8 Vantage|
|Required License|A-Spec: National B License|

|||||
|---|---|---|---|
|1st|2,000 cr|4th|400 cr|
|2nd|1,000 cr|5th|300 cr|
|3rd|500 cr|6th|200 cr|

- [ ] 2003 Aston Martin DB9 Coupe

  - [ ] `[ | | | | | | | ]` 2 laps - Fuji Speedway 90's
  - [ ] `[ | | | | | | | ]` 3 laps - Hong Kong
  - [ ] `[ | | | | | | | ]` 2 laps - Autumn Ring

### 17.2 Lotus: Elise Trophy

|||
|---|---|
|Game Mode|Championship Race|
|Tires|No restrictions|
|Cars Permitted|Lotus Elise 111R|
||Lotus Elise Type 72|
||Lotus Elise|
||Lotus Motor Sport Elise|
||Lotus Elise Sport 190|
||etc.|
|Required License|A-Spec: National A License|

|||||
|---|---|---|---|
|1st|7,500 cr|4th|1,000 cr|
|2nd|5,000 cr|5th|500 cr|
|3rd|2,500 cr|6th|300 cr|

|||
|---|---|
|Winning Prize|30,000 cr|

- [ ] 2001 Lotus Elise Type 72

  - [ ] `[ | | | | | | | ]` 1 lap  - Tokyo Route 246
  - [ ] `[ | | | | | | | ]` 2 laps - Autumn Ring Reverse
  - [ ] `[ | | | | | | | ]` 2 laps - Special Stage Route 5 Reverse
  - [ ] `[ | | | | | | | ]` 2 laps - Hong Kong
  - [ ] `[ | | | | | | | ]` 2 laps - Apricot Hill Raceway

### 17.3 Lotus: Lotus Classics

|||
|---|---|
|Game Mode|Championship Race|
|Tires|No restrictions|
|Cars Permitted|Lotus Europa Special|
||Lotus Elan S1|
|Required License|None|

|||||
|---|---|---|---|
|1st|2,000 cr|4th|300 cr|
|2nd|500 cr|5th|200 cr|
|3rd|400 cr|6th|100 cr|

|||
|---|---|
|Winning Prize|10,000 cr|

- [ ] 1962 Lotus Elan S1

  - [ ] `[ | | | | | | | ]` 2 laps - Hong Kong Reverse
  - [ ] `[ | | | | | | | ]` 2 laps - Mazda Raceway Laguna Seca
  - [ ] `[ | | | | | | | ]` 2 laps - El Capitan
  - [ ] `[ | | | | | | | ]` 3 laps - Tsukuba Circuit
  - [ ] `[ | | | | | | | ]` 2 laps - Apricot Hill Raceway Reverse

### 17.4 Mini: Mini Mini Sports Meeting

|||
|---|---|
|Game Mode|Single Race|
|Tires|No restrictions|
|Cars Permitted|Mini COOPER-S|
||Mini COOPER|
||Mini ONE|
|Required License|None|

|||||
|---|---|---|---|
|1st|3,000 cr|4th|400 cr|
|2nd|1,000 cr|5th|300 cr|
|3rd|500 cr|6th|200 cr|

- [ ] 1970 Marcos Mini Marcos GT

  - [ ] `[ | | | | | | | ]` 2 laps - Opera Paris
  - [ ] `[ | | | | | | | ]` 1 lap  - Tokyo Route 246
  - [ ] `[ | | | | | | | ]` 3 laps - Clubman Stage Route 5 Reverse
  - [ ] `[ | | | | | | | ]` 2 laps - Seattle Circuit
  - [ ] `[ | | | | | | | ]` 2 laps - New York Reverse

### 17.5 MG: MG Festival

|||
|---|---|
|Game Mode|Single Race|
|Tires|No restrictions|
|Cars Permitted|MG TF 160|
||MGF|
|Required License|A-Spec: National A License|

|||||
|---|---|---|---|
|1st|3,000 cr|4th|400 cr|
|2nd|1,000 cr|5th|300 cr|
|3rd|500 cr|6th|200 cr|

- [ ] 1997 MG MGF

  - [ ] `[ | | | | | | | ]` 2 laps - Hong Kong Reverse
  - [ ] `[ | | | | | | | ]` 1 lap  - Suzuka Circuit
  - [ ] `[ | | | | | | | ]` 2 laps - Opera Paris
  - [ ] `[ | | | | | | | ]` 4 laps - Motorland
  - [ ] `[ | | | | | | | ]` 2 laps - Grand Valley East Reverse

### 17.6 Triumph: Spitfire Cup

|||
|---|---|
|Game Mode|Single Race|
|Tires|No restrictions|
|Cars Permitted|Triumph Spitfire 1500|
|Required License|A-Spec: National A License|

|||||
|---|---|---|---|
|1st|3,000 cr|4th|400 cr|
|2nd|1,000 cr|5th|300 cr|
|3rd|500 cr|6th|200 cr|

- [ ] 1974 Triumph Spitfire 1500

  - [ ] `[ | | | | | | | ]` 2 laps - El Capitan
  - [ ] `[ | | | | | | | ]` 3 laps - Twin Ring Motegi (West Short Course)
  - [ ] `[ | | | | | | | ]` 4 laps - Autumn Ring Mini Reverse
  - [ ] `[ | | | | | | | ]` 3 laps - Clubman Stage Route 5
  - [ ] `[ | | | | | | | ]` 2 laps - Seoul Central Reverse

### 17.7 TVR: Black Pool Racers

|||
|---|---|
|Game Mode|Single Race|
|Tires|No Restrictions|
|Cars Permitted|TVR T350C|
||TVR Tamora|
||TVR Tuscan Speed 6|
||TVR Cerbera Speed Six|
||TVR Griffith 500|
||etc.|
|Required License|A-Spec: National A License|

|||||
|---|---|---|---|
|1st|3,000 cr|4th|400 cr|
|2nd|1,000 cr|5th|300 cr|
|3rd|500 cr|6th|200 cr|

- [ ] 2000 TVR Cerbera Speed 12

  - [ ] `[ | | | | | | | ]` 5 laps - Suzuka Circuit (East Course)
  - [ ] `[ | | | | | | | ]` 3 laps - Special Stage Route 5
  - [ ] `[ | | | | | | | ]` 3 laps - Mazda Raceway Laguna Seca
  - [ ] `[ | | | | | | | ]` 4 laps - Opera Paris
  - [ ] `[ | | | | | | | ]` 3 laps - Fuji Speedway 2005

-------------------------------------------------------------------------------
## 18 One-make Races: USA
-------------------------------------------------------------------------------

### 18.1 Chevrolet: Vette! Vette! Vette!

|||
|---|---|
|Game Mode|Single Race|
|Tires|No restrictions|
|Cars Permitted|Chevrolet Corvette Z06 (C5)|
||Chevrolet Corvette Grand Sport (C4)|
||Chevrolet ZR-1 (C4)|
||Chevrolet Stingray L46 350|
||Chevrolet Corvette Coupe (C2)|
||etc.|
|Required License|A-Spec: International B License|

|||||
|---|---|---|---|
|1st|10,000 cr|4th|3,000 cr|
|2nd|6,000 cr|5th|1,000 cr|
|3rd|4,000 cr|6th|500 cr|

- [ ] 1963 Chevrolet Corvette Z06 (C2) Race Car

  - [ ] `[ | | | | | | | ]` 4 laps - Seattle Circuit
  - [ ] `[ | | | | | | | ]` 4 laps - Mazda Raceway Laguna Seca
  - [ ] `[ | | | | | | | ]` 4 laps - New York Reverse
  - [ ] `[ | | | | | | | ]` 4 laps - Infineon Raceway (Sports Car Course)
  - [ ] `[ | | | | | | | ]` 4 laps - El Capitan

### 18.2 Chevrolet: Camaro Meeting

|||
|---|---|
|Game Mode|Single Race|
|Tires|No restrictions|
|Cars Permitted|Chevrolet Camaro SS|
||Chevrolet Camaro Z28 Coupe|
||Chevrolet Camaro IROC-Z Concept|
||Chevrolet Z28 302|
||Chevrolet Camaro SS|
||etc.|
|Required License|A-Spec: National B License|

|||||
|---|---|---|---|
|1st|2,000 cr|4th|400 cr|
|2nd|1,000 cr|5th|300 cr|
|3rd|500 cr|6th|200 cr|

- [ ] 1988 Chevrolet Camaro IROC-Z Concept

  - [ ] `[ | | | | | | | ]` 4 laps - Seoul Central
  - [ ] `[ | | | | | | | ]` 3 laps - El Capitan Reverse
  - [ ] `[ | | | | | | | ]` 3 laps - Mid-Field Raceway

### 18.3 Chrysler: Crossfire Trophy

|||
|---|---|
|Game Mode|Single Race|
|Tires|No restrictions|
|Cars Permitted|Chrysler Crossfire|
|Required License|A-Spec: National A License|

|||||
|---|---|---|---|
|1st|7,500 cr|4th|1,000 cr|
|2nd|5,000 cr|5th|500 cr|
|3rd|2,500 cr|6th|300 cr|

- [ ] 2000 Dodge Viper GTSR Concept

  - [ ] `[ | | | | | | | ]` 2 laps - Seattle Circuit Reverse
  - [ ] `[ | | | | | | | ]` 3 laps - Twin Ring Motegi (Super Speedway)
  - [ ] `[ | | | | | | | ]` 2 laps - Trial Mountain Reverse

### 18.4 Saleen: Saleen S7 Club

|||
|---|---|
|Game Mode|Single Race|
|Tires|No restrictions|
|Cars Permitted|Saleen S7|
|Required License|A-Spec: International B License|

|||||
|---|---|---|---|
|1st|10,000 cr|4th|3,000 cr|
|2nd|6,000 cr|5th|1,000 cr|
|3rd|4,000 cr|6th|500 cr|

- [ ] 2022 Nike One

  - [ ] `[ | | | | | | | ]` 2 laps - Circuit de la Sarthe I
  - [ ] `[ | | | | | | | ]` 4 laps - El Capitan
  - [ ] `[ | | | | | | | ]` 4 laps - Special Stage Route 5 Reverse
  - [ ] `[ | | | | | | | ]` 3 laps - Suzuka Circuit
  - [ ] `[ | | | | | | | ]` 4 laps - Infineon Raceway (Sports Car Course)

### 18.5 Shelby: Shelby Cobra Cup

|||
|---|---|
|Game Mode|Single Race|
|Tires|No restrictions|
|Cars Permitted|Shelby Series 1 Super Charged|
||Shelby Cobra 427|
||Shelby Mustang G.T. 350R|
|Required License|A-Spec: International B License|

|||||
|---|---|---|---|
|1st|10,000 cr|4th|3,000 cr|
|2nd|6,000 cr|5th|1,000 cr|
|3rd|4,000 cr|6th|500 cr|

- [ ] 1965 Shelby Mustang G.T. 350R

  - [ ] `[ | | | | | | | ]` 2 laps - Seattle Circuit Reverse
  - [ ] `[ | | | | | | | ]` 2 laps - High-Speed Ring Reverse
  - [ ] `[ | | | | | | | ]` 2 laps - New York
  - [ ] `[ | | | | | | | ]` 2 laps - Mazda Raceway Laguna Seca
  - [ ] `[ | | | | | | | ]` 2 laps - Trial Mountain Reverse

## 19 Car List

The following is a list of cars which feature in [[Gran Turismo 4, ordered by Manufacturer:

Source: https://gran-turismo.fandom.com/wiki/Gran_Turismo_4/Car_List

==Keys==
*[DS] the car can race on dirt and snow courses
*[P] the car can also/only be obtained as a prize
*[S] the car has a special color available
*[U] the car appears in the used car dealership

== [ ] AC Cars ==
[ ] AC Cars 427 S/C '66

== [ ] Acura ==
[ ] Acura CL 3.2 Type-S '01
[ ] Acura CL 3.2 Type-S '03
[ ] Acura DN-X '02 [P]
[ ] Acura HSC '04 [P]
[ ] Acura Integra Type-R '01
[ ] Acura NSX '04
[ ] Acura NSX '91
[ ] Acura NSX Coupe '97
[ ] Acura RSX Type-S '04

== [ ] Alfa Romeo ==
[ ] Alfa Romeo 147 2.0 Twinspark '02
[ ] Alfa Romeo 147 GTA '02
[ ] Alfa Romeo 155 2.5 V6 TI '93 [P]
[ ] Alfa Romeo 156 2.5 V6 24V '98
[ ] Alfa Romeo 166 2.5 V6 24V Sportronic '98
[ ] Alfa Romeo Giulia Sprint GTA 1600 '65 [P]
[ ] Alfa Romeo Giulia Sprint Speciale '63 [P]
[ ] Alfa Romeo GT 3.2 V6 24V '04 
[ ] Alfa Romeo GTV 3.0 V6 24V '01
[ ] Alfa Romeo Spider 1600 Duetto '66
[ ] Alfa Romeo Spider 3.0i V6 24V '01

== [ ] Alpine ==
[ ] Alpine A110 1600S '73 [P]
[ ] Alpine A310 1600VE '73 [P]

== [ ] Amuse ==
[ ] Amuse Carbon R (R34) '04
[ ] Amuse S2000 GT1 '04
[ ] Amuse S2000 R1 '04
[ ] Amuse S2000 Street Version '04

== [ ] ASL ==
[ ] ASL ARTA Garaiya (JGTC) '03
[ ] ASL Garaiya '02

== [ ] Aston Martin ==
[ ] Aston Martin DB7 Vantage Coupe '00
[ ] Aston Martin DB9 Coupe '03 [P] [S]
[ ] Aston Martin V8 Vantage '99
[ ] Aston Martin Vanquish '04

== [ ] Audi ==
[ ] Audi A2 1.4 '02
[ ] Audi A3 3.2 quattro '03
[ ] Audi A4 Touring Car '04
[ ] Auto Union V16 Type C Streamline '37 [P]
[ ] Audi Le Mans quattro '03 [P]
[ ] Audi Nuvolari quattro '03 [P]
[ ] Audi Pikes Peak quattro '03 [P]
[ ] Audi R8 Race Car '01 [P]
[ ] Audi RS 4 '01
[ ] Audi RS 6 '02
[ ] Audi RS 6 Avant '02
[ ] Audi S3 '02
[ ] Audi S4 '03
[ ] Audi S4 '98
[ ] Audi TT Coupe 1.8T quattro '00
[ ] Audi TT Coupe 3.2 quattro '03

== [ ] Autobianchi ==
[ ] Autobianchi A112 Abarth '79 [P]

== [ ] Bentley ==
[ ] Bentley Speed 8 Race Car '03 [P]

== [ ] Blitz ==
[ ] Blitz ER34 D1 spec 2004 (D1GP) '04

== [ ] BMW ==
[ ] BMW 120d '04
[ ] BMW 120i '04
[ ] BMW 2002 Turbo '73 [P]
[ ] BMW 320i Touring Car '03
[ ] BMW 330i '05
[ ] BMW M Coupe '98
[ ] BMW M3 '04
[ ] BMW M3 CSL '03
[ ] BMW M3 GTR '03 [P]
[ ] BMW M3 GTR Race Car '01 [P]
[ ] BMW M5 '05
[ ] BMW McLaren F1 GTR Race Car '97 [P]
[ ] BMW V12 LMR Race Car '99
[ ] BMW Z4 '03

== [ ] Buick ==
[ ] Buick GNX '87
[ ] Buick Special '62

== [ ] Cadillac ==
[ ] Cadillac CIEN '02 [P]

== [ ] Callaway ==
[ ] Callaway C12 '03

== [ ] Caterham ==
[ ] Caterham Seven Fire Blade '02

== [ ] Chaparral ==
[ ] Chaparral 2D Race Car '67 [P]
[ ] Chaparral 2J Race Car '70

== [ ] Chevrolet ==
[ ] Chevrolet Camaro IROC-Z Concept '88 [P]
[ ] Chevrolet Camaro LM Race Car '01 [P]
[ ] Chevrolet Camaro SS '00
[ ] Chevrolet Camaro SS '69
[ ] Chevrolet Camaro Z28 302 '69
[ ] Chevrolet Camaro Z28 Coupe '97
[ ] Chevrolet Chevelle SS 454 '70 [P]
[ ] Chevrolet Corvette C5R (C5) '00
[ ] Chevrolet Corvette Convertible (C1) '54 [P]
[ ] Chevrolet Corvette Coupe (C2) '63
[ ] Chevrolet Corvette GRAND SPORT (C4) '96
[ ] Chevrolet Corvette Stingray L46 350 (C3) '69
[ ] Chevrolet Corvette Z06 (C2) Race Car '63 [P]
[ ] Chevrolet Corvette Z06 (C5) '00
[ ] Chevrolet Corvette ZR-1 (C4) '90
[ ] Chevrolet Silverado SST Concept '02 [P]
[ ] Chevrolet SSR '03

== [ ] Chrysler ==
[ ] Chrysler 300C '05 
[ ] Chrysler Crossfire '04
[ ] Chrysler Prowler '02 [P]
[ ] Chrysler PT-Cruiser '00

== [ ] Citroën ==
[ ] Citroën 2CV Type A '54 [P] [S]
[ ] Citroën C3 1.6 '02
[ ] Citroën C5 V6 Exclusive '03
[ ] Citroën Xantia 3.0i V6 Exclusive '00
[ ] Citroën Xsara Rally Car '99
[ ] Citroën Xsara VTR '03

== [ ] Cizeta ==
[ ] Cizeta V16T '94 [P]

== [ ] Daihatsu ==
[ ] Daihatsu Copen Active Top '02
[ ] Daihatsu Copen Detachable Top '02
[ ] Daihatsu Midget '63 [P]
[ ] Daihatsu Midget II D-type '98
[ ] Daihatsu MIRA TR-XX Avanzato R '97 
[ ] Daihatsu MOVE Custom RS Limited '02
[ ] Daihatsu MOVE CX '95
[ ] Daihatsu MOVE SR-XX 2WD '97
[ ] Daihatsu MOVE SR-XX 4WD '97
[ ] Daihatsu STORIA CX 2WD '98 
[ ] Daihatsu STORIA CX 4WD '98 
[ ] Daihatsu STORIA X4 '00 [P] [S]

== [ ] DMC ==
[ ] DMC DeLorean S2 '04 [P]

== [ ] Dodge ==
[ ] Dodge Charger 440 R/T '70
[ ] Dodge Charger Super Bee 426 Hemi '71 [P]
[ ] Dodge RAM 1500 LARAMIE Hemi Quad Cab '04
[ ] Dodge SRT4 '03
[ ] Dodge VIPER GTS '99
[ ] Dodge VIPER GTS-R Team Oreca Race Car '00 
[ ] Dodge VIPER GTSR Concept '00 [P]
[ ] Dodge VIPER SRT10 '03

== [ ] Dome ==
[ ] Dome ZERO '78 [P]

== [ ] Eagle ==
[ ] Eagle Talon Esi '97

== [ ] Fiat ==
[ ] Fiat 500F '65
[ ] Fiat 500L '69
[ ] Fiat 500R '72
[ ] Fiat Barchetta Giovane Due '00
[ ] Fiat Coupe Turbo Plus '00
[ ] Fiat Panda Super i.e. '90
[ ] Fiat Punto HGT Abarth '00

== [ ] Ford ==
[ ] Ford Escort Rally Car '98 [P]
[ ] Ford FOCUS Rally Car '99
[ ] Ford FOCUS RS '02
[ ] Ford FOCUS ST170 '03
[ ] Ford GT '02 [P]
[ ] Ford GT '05
[ ] Ford GT '05 [P]
[ ] Ford GT LM Race Car '02 [P]
[ ] Ford GT LM Race Car Spec II '04 [P]
[ ] Ford GT40 Race Car '69 [P]
[ ] Ford Ka '01
[ ] Ford Model T Tourer '15 [P]
[ ] Ford Mustang GT '05
[ ] Ford MUSTANG SVT Cobra R '00
[ ] Ford RS200 '84 [P]
[ ] Ford RS200 Rally Car '85 [P]
[ ] Ford SVT F-150 Lightning '03
[ ] Ford Taurus SHO '98

== [ ] Ford Australia ==
[ ] Ford Australia 2000 Falcon XR8

== [ ] FPV ==
[ ] FPV F6 Typhoon '04
[ ] FPV GT '04

== [ ] Gillet ==
[ ] Gillet Vertigo Race Car '04

== [ ] Ginetta ==
[ ] Ginetta G4 '64 [P]

== [ ] HKS ==
[ ] HKS GENKI HYPER SILVIA RS2 (D1GP) '04

== [ ] Holden ==
[ ] Holden Commodore SS '04
[ ] Holden Monaro CV8 '04

== [ ] Hommell ==
[ ] Hommell Berlinette R/S Coupe '99

== [ ] Honda ==
[ ] Honda 1300 Coupe 9 S '70
[ ] Honda ACCORD Coupe '88 
[ ] Honda ACCORD Coupe EX '03 
[ ] Honda ACCORD Euro-R '00
[ ] Honda ACCORD Euro-R '02
[ ] Honda ARTA NSX (JGTC) '00
[ ] Honda BALLADE SPORTS CR-X 1.5i '83
[ ] Honda BEAT '91
[ ] Honda BEAT Version F '92
[ ] Honda BEAT Version Z '93
[ ] Honda Castrol MUGEN NSX (JGTC) '00
[ ] Honda CITY Turbo II '83
[ ] Honda CIVIC 1500 3door 25i '83
[ ] Honda CIVIC 1500 3door CX '79
[ ] Honda CIVIC SiR-II (EG) '91
[ ] Honda CIVIC SiR-II (EG) '92
[ ] Honda CIVIC SiR-II (EG) '93
[ ] Honda CIVIC SiR-II (EG) '95
[ ] Honda CIVIC TYPE R (EK) '97
[ ] Honda CIVIC TYPE R (EK) '98
[ ] Honda CIVIC TYPE R (EP) '01 
[ ] Honda CIVIC TYPE R (EP) '04
[ ] Honda CR-X del Sol SiR '92
[ ] Honda CR-X SiR '90
[ ] Honda ELEMENT '03
[ ] Honda Fit W '01 
[ ] Honda Gathers Drider CIVIC Race Car '98
[ ] Honda Insight '99
[ ] Honda INTEGRA TYPE R (DC2) '95
[ ] Honda INTEGRA TYPE R (DC2) '98
[ ] Honda INTEGRA TYPE R (DC2) '99
[ ] Honda INTEGRA TYPE R (DC5) '03
[ ] Honda INTEGRA TYPE R Touring Car '02
[ ] Honda LIFE STEP VAN '72 [P]
[ ] Honda LOCTITE MUGEN NSX (JGTC) '01
[ ] Honda Mobil 1 NSX (JGTC) '01
[ ] Honda MUGEN MOTUL CIVIC Si Race Car '87 [P]
[ ] Honda N360 '67
[ ] Honda NSX '01
[ ] Honda NSX '90
[ ] Honda NSX '93
[ ] Honda NSX '95
[ ] Honda NSX '97
[ ] Honda NSX '99
[ ] Honda NSX Type R '02
[ ] Honda NSX Type R '92
[ ] Honda NSX Type S '01
[ ] Honda NSX Type S '97
[ ] Honda NSX Type S '99
[ ] Honda NSX Type S Zero '97
[ ] Honda NSX Type S Zero '99
[ ] Honda NSX-R Concept '01 [P]
[ ] Honda NSX-R Prototype LM Race Car '02 [P]
[ ] Honda NSX-R Prototype LM Road Car '02 [P]
[ ] Honda Odyssey '03
[ ] Honda PRELUDE Si VTEC '91
[ ] Honda PRELUDE SiR '96
[ ] Honda PRELUDE SiR S spec '98
[ ] Honda PRELUDE Type S '96
[ ] Honda PRELUDE Type S '98
[ ] Honda RAYBRIG NSX (JGTC) '00
[ ] Honda S2000 '01
[ ] Honda S2000 '04
[ ] Honda S2000 '99
[ ] Honda S2000 LM Race Car '01
[ ] Honda S2000 Type V '00
[ ] Honda S2000 Type V '01
[ ] Honda S2000 Type V '03 
[ ] Honda S500 '63 [P]
[ ] Honda S600 '64
[ ] Honda S800 '66
[ ] Honda S800 RSC Race Car '68 [P]
[ ] Honda TAKATA DOME NSX (JGTC) '03
[ ] Honda TODAY G '85
[ ] Honda Z ACT '70

== [ ] HPA Motorsports ==
[ ] HPA Motorsports Stage II R32 '04

== [ ] Hyundai ==
[ ] Hyundai Accent Rally Car '01 
[ ] Hyundai Clix '01 [P]
[ ] Hyundai HCD6 '01 [P]
[ ] Hyundai Tiburon GT '01 

== [ ] Infiniti ==
[ ] Infiniti FX45 Concept '02 [P]
[ ] Infiniti G20 '90 
[ ] Infiniti G35 Coupe '03 
[ ] Infiniti G35 SEDAN '03

== [ ] Isuzu ==
[ ] Isuzu 117 Coupe '68 [P] [S]
[ ] Isuzu Bellett 1600 GT-R '69
[ ] Isuzu PIAZZA XE '81

== [ ] Jaguar ==
[ ] Jaguar E-Type Coupe '61 [P]
[ ] Jaguar S-Type R '02
[ ] Jaguar XJ220 '92
[ ] Jaguar XJ220 LM Race Car '01 [P]
[ ] Jaguar XJR-9 Race Car '88 [P]
[ ] Jaguar XKR Coupe '99
[ ] Jaguar XKR R-Performance '02

== [ ] Jay Leno ==
[ ] Jay Leno Tank Car '03 [P]

== [ ] Jensen ==
[ ] Jensen Interceptor MkIII '74 [P] [U]

== [ ] Lancia ==
[ ] Lancia DELTA HF Integrale Evoluzione '91
[ ] Lancia DELTA HF Integrale Rally Car '92 [ P]
[ ] Lancia Delta S4 Rally Car '85 [P]
[ ] Lancia STRATOS '73
[ ] Lancia STRATOS Rally Car '77 [P]

== [ ] Land Rover ==
[ ] Land Rover Range Stormer Concept '04 [P]

== [ ] Lexus ==
[ ] Lexus GS 300 '00
[ ] Lexus GS 300 '91
[ ] Lexus GS 300 Vertex Edition (J) '00
[ ] Lexus IS 200 '98
[ ] Lexus IS 200 (J) '98
[ ] Lexus IS 300 Sport Cross '01
[ ] Lexus SC 300 '97
[ ] Lexus SC 430 '01

== [ ] Lister ==
[ ] Lister Storm V12 Race Car '99 [P]

== [ ] Lotus ==
[ ] Lotus Carlton '90
[ ] Lotus Elan S1 '62 [P]
[ ] Lotus Elise '00
[ ] Lotus Elise 111R '04
[ ] Lotus Elise 111S '03
[ ] Lotus Elise Sport 190 '98
[ ] Lotus Elise Type 72 '01 [P]
[ ] Lotus Esprit Sport 350 '00
[ ] Lotus Esprit Turbo HC '87 [P]
[ ] Lotus Esprit V8 '02
[ ] Lotus Esprit V8 GT '98
[ ] Lotus Esprit V8 SE '98
[ ] Lotus Europa Special '71 [P]
[ ] Lotus Motor Sport Elise '99

== [ ] Marcos ==
[ ] Marcos Mini Marcos GT '70 [P]

== [ ] Mazda ==
[ ] Mazda 110S (L10A) '67 [P]
[ ] Mazda 110S (L10B) '68 
[ ] Mazda 787B Race Car '91 [S] A black version of the car is available on the 100th used car cycle (days 694-700) in Early '90s Used Cars Showroom
[ ] Mazda Autozam AZ-1 '92
[ ] Mazda BP FALKEN RX-7 (D1GP) '03 [P]
[ ] Mazda Carol 360 Deluxe '62
[ ] Mazda DEMIO (J) '99 
[ ] Mazda DEMIO SPORT '03 
[ ] Mazda KUSABI '03 [P]
[ ] Mazda Mazda 323F '93 
[ ] Mazda Mazda6 5-door '03 
[ ] Mazda Mazda6 Concept '01 [P]
[ ] Mazda Mazda6 Touring Car '02 [P]
[ ] Mazda Mazdaspeed6 '05 [P]
[ ] Mazda MX-5 Miata (NA) '89 
[ ] Mazda MX-5 Miata 1.8 RS (NB, J) '98 
[ ] Mazda MX-5 Miata 1600 NR-A (NB, J) '04 
[ ] Mazda MX-5 Miata 1800 RS (NB, J) '00 
[ ] Mazda MX-5 Miata 1800 RS (NB, J) '04 
[ ] Mazda MX-5 Miata J-Limited (NA, J) '91 
[ ] Mazda MX-5 Miata J-Limited II (NA, J) '93 
[ ] Mazda MX-5 Miata S-Special Type I (NA, J) '95 
[ ] Mazda MX-5 Miata SR-Limited (NA, J) '97 
[ ] Mazda MX-5 Miata V-Special Type II (NA, J) '93 
[ ] Mazda MX-5 Miata VR-Limited (NA, J) '95 
[ ] Mazda MX-Crossport Concept '05 [P]
[ ] Mazda Protegé '02 
[ ] Mazda RX-7 GT-Limited (FC, J) '85 
[ ] Mazda RX-7 GT-X (FC, J) '90 
[ ] Mazda RX-7 LM Race Car '01 [P]
[ ] Mazda RX-7 Spirit R Type A (FD) '02
[ ] Mazda RX-7 Type R (FD, J) '91 
[ ] Mazda RX-7 Type R (FD, J) '93 
[ ] Mazda RX-7 Type R Bathurst R (FD) '01
[ ] Mazda RX-7 Type R-S (FD, J) '95 
[ ] Mazda RX-7 Type RS (FD, J) '96 
[ ] Mazda RX-7 Type RS (FD) '00
[ ] Mazda RX-7 Type RS (FD) '98
[ ] Mazda RX-7 Type RS-R (FD) '97
[ ] Mazda RX-7 Type RZ (FD, J) '92 
[ ] Mazda RX-7 Type RZ (FD, J) '93 
[ ] Mazda RX-7 Type RZ (FD, J) '95 
[ ] Mazda RX-7 Type RZ (FD, J) '96 
[ ] Mazda RX-7 Type RZ (FD) '00
[ ] Mazda RX-8 '03
[ ] Mazda RX-8 Concept (Type-I) '01 [P]
[ ] Mazda RX-8 Concept (Type-II) '01 [P]
[ ] Mazda RX-8 Concept LM Race Car '01 [P]
[ ] Mazda RX-8 Type E '03
[ ] Mazda RX-8 Type S '03

== [ ] Mercedes-Benz ==
[ ] Mercedes-Benz 190 E 2.5 - 16 Evolution II '91
[ ] Mercedes-Benz 300 SL Coupe '54 [P]
[ ] Mercedes-Benz A 160 Avantgarde '98
[ ] Mercedes-Benz AMG Mercedes 190 E 2.5 - 16 Evolution II Touring Car '92 [P]
[ ] Mercedes-Benz AMG Mercedes CLK-GTR Race Car '98 [P]
[ ] Mercedes-Benz Benz Patent Motor Wagen '86 [P]
[ ] Mercedes-Benz CL 600 '00
[ ] Mercedes-Benz CLK 55 AMG '00
[ ] Mercedes-Benz CLK Touring Car '00
[ ] Mercedes-Benz Daimler Motor Carriage '86 [P]
[ ] Mercedes-Benz E 55 AMG '02
[ ] Mercedes-Benz Sauber Mercedes C 9 Race Car '89 [P]
[ ] Mercedes-Benz SL 500 (R129) '98
[ ] Mercedes-Benz SL 500 (R230) '02
[ ] Mercedes-Benz SL 55 AMG (R230) '02
[ ] Mercedes-Benz SL 600 (R129) '98
[ ] Mercedes-Benz SL 600 (R230) '04
[ ] Mercedes-Benz SL 65 AMG (R230) '04
[ ] Mercedes-Benz SLK 230 Kompressor '98
[ ] Mercedes-Benz SLR McLaren '03

== [ ] Mercury ==
[ ] Mercury Cougar XR-7 '67 [P]

== [ ] MG ==
[ ] MG MGF '97 [P]
[ ] MG TF160 '03

== [ ] Mine's ==
[ ] Mine's Lancer Evolution VI '00
[ ] Mine's Skyline GT-R• N1 V• spec (R34) '00

== [ ] Mini ==
[ ] MINI COOPER '02
[ ] MINI COOPER-S '02
[ ] MINI ONE '02

== [ ] Mitsubishi ==
[ ] Mitsubishi 3000GT MR (J) '95 
[ ] Mitsubishi 3000GT MR (J) '98 
[ ] Mitsubishi 3000GT SL (J) '95 
[ ] Mitsubishi 3000GT SL (J) '96 
[ ] Mitsubishi 3000GT SL (J) '98 
[ ] Mitsubishi 3000GT VR-4 (J) '98 
[ ] Mitsubishi 3000GT VR-4 Turbo (J) '95 
[ ] Mitsubishi 3000GT VR-4 Turbo (J) '96 
[ ] Mitsubishi AIRTREK Turbo-R '02
[ ] Mitsubishi COLT 1.5 Sport X Version '02
[ ] Mitsubishi CZ-3 Tarmac '01 [P]
[ ] Mitsubishi CZ-3 Tarmac Rally Car '02 [P]
[ ] Mitsubishi ECLIPSE GT '06 
[ ] Mitsubishi ECLIPSE GT '95
[ ] Mitsubishi ECLIPSE Spyder GTS '03
[ ] Mitsubishi FTO GP Version R '97
[ ] Mitsubishi FTO GP Version R '99
[ ] Mitsubishi FTO GPX '94
[ ] Mitsubishi FTO GPX '97
[ ] Mitsubishi FTO GPX '99
[ ] Mitsubishi FTO GR '94
[ ] Mitsubishi FTO GR '97
[ ] Mitsubishi FTO Super Touring Car '97 [P]
[ ] Mitsubishi Galant 2.0 DOHC Turbo VR-4 '89
[ ] Mitsubishi GALANT GTO MR '70
[ ] Mitsubishi HSR-II Concept '89 [P]
[ ] Mitsubishi i '03 [P]
[ ] Mitsubishi Lancer 1600 GSR '73
[ ] Mitsubishi Lancer 1600 GSR Rally Car '74 [P]
[ ] Mitsubishi Lancer Evolution GSR '92
[ ] Mitsubishi Lancer Evolution II GSR '94
[ ] Mitsubishi Lancer Evolution III GSR '95
[ ] Mitsubishi Lancer Evolution IV GSR '96
[ ] Mitsubishi Lancer Evolution IV Rally Car '97 [P]
[ ] Mitsubishi Lancer Evolution Super Rally Car '03
[ ] Mitsubishi Lancer Evolution V GSR '98
[ ] Mitsubishi Lancer Evolution VI GSR '99
[ ] Mitsubishi Lancer Evolution VI GSR TOMMI MAKINEN Edition '00
[ ] Mitsubishi Lancer Evolution VI Rally Car '99
[ ] Mitsubishi Lancer Evolution VI RS '99
[ ] Mitsubishi Lancer Evolution VI RS TOMMI MAKINEN Edition '00
[ ] Mitsubishi Lancer Evolution VII GSR '01
[ ] Mitsubishi Lancer Evolution VII GT-A '02
[ ] Mitsubishi Lancer Evolution VII RS '01
[ ] Mitsubishi Lancer Evolution VIII GSR '03
[ ] Mitsubishi Lancer Evolution VIII MR GSR '04
[ ] Mitsubishi Lancer Evolution VIII RS '03
[ ] Mitsubishi Lancer EX 1800GSR IC Turbo '83
[ ] Mitsubishi LEGNUM VR-4 Type V '98
[ ] Mitsubishi MINICA DANGAN ZZ '89
[ ] Mitsubishi MIRAGE 1400GLX '78
[ ] Mitsubishi Mirage CYBORG ZR '97
[ ] Mitsubishi PAJERO Evolution Rally Raid Car '03 [P]
[ ] Mitsubishi PAJERO Rally Raid Car '85 [P]
[ ] Mitsubishi STARION 4WD Rally Car '84 [P]

== [ ] Mugen ==
[ ] Mugen S2000 '00

== [ ] Nike ==
[ ] Nike One 2022 [P]

== [ ] Nismo ==
[ ] NISMO 270R '94 [P]
[ ] NISMO 400R '96 [P]
[ ] NISMO Fairlady Z S-tune concept by GRANTURISMO (Z33) '02
[ ] NISMO Fairlady Z Z-tune (Z33) '03
[ ] NISMO GT-R LM Road Going Version '95 [P]
[ ] NISMO Skyline GT-R R-tune (R34) '99
[ ] NISMO Skyline GT-R S-tune (R32) '00

== [ ] Nissan ==
[ ] Nissan 240RS Rally Car '85 [P]
[ ] Nissan 240SX '96 
[ ] Nissan 240SX (S14) '96 
[ ] Nissan 240ZG (HS30) '71 [P]
[ ] Nissan 300ZX 2by2 (Z32) '98 
[ ] Nissan 300ZX 2seater (Z32) '89 
[ ] Nissan 300ZX 2seater (Z32) '98 
[ ] Nissan 350Z (Z33) '03
[ ] Nissan 350Z Concept LM Race Car '02 [P]
[ ] Nissan 350Z Gran Turismo 4 Limited Edition (Z33) '05
[ ] Nissan 350Z Roadster (Z33) '03
[ ] Nissan Be-1 '87
[ ] Nissan BLUEBIRD 1600 Deluxe (510) '69
[ ] Nissan BLUEBIRD Hardtop 1800SSS (910) '79
[ ] Nissan BLUEBIRD Rally Car (510) '69 [P]
[ ] Nissan C-WEST RAZO SILVIA (JGTC) '01
[ ] Nissan CALSONIC SKYLINE (JGTC) '00
[ ] Nissan CALSONIC SKYLINE GT-R Race Car '93 [P]
[ ] Nissan CUBE EX (FF/CVT) '02
[ ] Nissan CUBE X '98
[ ] Nissan EXA CANOPY L.A.Version Type S (N13) '88
[ ] Nissan Fairlady 2000 (SR311) '68
[ ] Nissan Fairlady Z 280Z-L 2seater (S130) '78
[ ] Nissan Fairlady Z 300ZX (Z31) '83
[ ] Nissan Fairlady Z Version ST (Z33) Option Wheel '02
[ ] Nissan FALKEN☆GT-R Race Car '04
[ ] Nissan GRAN TURISMO SKYLINE GT-R '01 [P]
[ ] Nissan GRAN TURISMO SKYLINE GT-R (PaceCar) '01 [P]
[ ] Nissan GT-R Concept (TokyoShow) '01 [P]
[ ] Nissan GT-R Concept LM Race Car '02 [P]
[ ] Nissan LOCTITE ZEXEL GT-R (JGTC) '00
[ ] Nissan March G♯ '99
[ ] Nissan MICRA '03 
[ ] Nissan mm-R Cup Car '01 [P]
[ ] Nissan MOTUL PITWORK Z (JGTC) '04 [P]
[ ] Nissan OPTION Stream Z '04 [P]
[ ] Nissan PAO '89
[ ] Nissan PENNZOIL Nismo GT-R (JGTC) '99
[ ] Nissan PENNZOIL ZEXEL GT-R (JGTC) '01
[ ] Nissan PRIMERA 20V '01 
[ ] Nissan R390 GT1 Race Car '98 [S] A black version of the car is available on the 100th used car cycle (days 694-700) in Late '90s Used Cars Showroom
[ ] Nissan R390 GT1 Road Car '98
[ ] Nissan R89C Race Car '89 [P]
[ ] Nissan R92CP Race Car '92 [P] [S] A black version of the car is available on the 100th used car cycle (days 694-700) in Late '90s Used Cars Showroom
[ ] Nissan SILVIA (CSP311) '65
[ ] Nissan SILVIA 240RS (S110) '83
[ ] Nissan SILVIA K's (S13) '88
[ ] Nissan SILVIA K's (S13) '91
[ ] Nissan SILVIA K's AERO (S14) '93
[ ] Nissan SILVIA Q's (S13) '88
[ ] Nissan SILVIA Q's (S13) '91
[ ] Nissan SILVIA Q's AERO (S14) '93
[ ] Nissan SILVIA Q's AERO (S14) '96
[ ] Nissan SILVIA Spec R AERO (S15) '99
[ ] Nissan SILVIA Spec S AERO (S15) '99
[ ] Nissan SILVIA Varietta (S15) '00
[ ] Nissan SKYLINE 1500Deluxe (S50D-1) '63
[ ] Nissan SKYLINE 2000GT-B (S54A) '67 [P]
[ ] Nissan SKYLINE 2000GT-R (KPGC110) '73
[ ] Nissan SKYLINE GT-R (R32) '89
[ ] Nissan SKYLINE GT-R (R32) '91
[ ] Nissan SKYLINE GT-R (R33) '95
[ ] Nissan SKYLINE GT-R (R33) '96
[ ] Nissan SKYLINE GT-R (R33) '97
[ ] Nissan SKYLINE GT-R (R34) '00
[ ] Nissan SKYLINE GT-R (R34) '99
[ ] Nissan SKYLINE GT-R M-spec (R34) '01
[ ] Nissan SKYLINE GT-R M-spec Nür (R34) '02
[ ] Nissan SKYLINE GT-R N1 (R32) '91
[ ] Nissan SKYLINE GT-R N1 (R33) '95
[ ] Nissan SKYLINE GT-R Special Color Midnight Purple II (R34) '99
[ ] Nissan SKYLINE GT-R Special Color Midnight Purple III (R34) '00
[ ] Nissan SKYLINE GT-R V-spec (R34) '99
[ ] Nissan SKYLINE GT-R V-spec II (R34) '00
[ ] Nissan SKYLINE GT-R V-spec II N1 (R34) '00
[ ] Nissan SKYLINE GT-R V-spec II Nür (R34) '02
[ ] Nissan SKYLINE GT-R V-spec N1 (R34) '99
[ ] Nissan SKYLINE GT-R Vspec (R32) '93
[ ] Nissan SKYLINE GT-R Vspec (R33) '95
[ ] Nissan SKYLINE GT-R Vspec (R33) '96
[ ] Nissan SKYLINE GT-R Vspec (R33) '97
[ ] Nissan SKYLINE GT-R Vspec II (R32) '94
[ ] Nissan SKYLINE GT-R Vspec LM Limited (R33) '96
[ ] Nissan SKYLINE GT-R Vspec N1 (R32) '93
[ ] Nissan SKYLINE GTS-R (R31) '87
[ ] Nissan SKYLINE GTS-t Type M (R32) '89
[ ] Nissan SKYLINE GTS-t Type M (R32) '91
[ ] Nissan SKYLINE GTS25 Type S (R32) '91
[ ] Nissan SKYLINE Hard Top 2000 RS-X Turbo C (R30) '84
[ ] Nissan SKYLINE Hard Top 2000 Turbo RS (R30) '83
[ ] Nissan SKYLINE Hard Top 2000GT-R (KPGC10) '70 [P]
[ ] Nissan SKYLINE SEDAN 300GT '01
[ ] Nissan SKYLINE SEDAN 350GT-8 '02
[ ] Nissan SKYLINE Sport Coupe (BLRA-3) '62
[ ] Nissan STAGEA 25t RS FOUR S '98
[ ] Nissan STAGEA 260RS AutechVersion '98
[ ] Nissan XANAVI HIROTO GT-R (JGTC) '01
[ ] Nissan XANAVI NISMO GT-R (JGTC) '03

==None==
[ ] SILEIGHTY '98 [P]

== [ ] Opel ==
[ ] Opel Astra Touring Car (Opel Team Phoenix) '00
[ ] Opel Calibra Touring Car '94 [P]
[ ] Opel Corsa Comfort 1.4 '01
[ ] Opel Speedster '00
[ ] Opel Speedster Turbo '00
[ ] Opel Tigra 1.6i '99
[ ] Opel Vectra 3.2 V6 '03

== [ ] Opera Performance ==
[ ] Opera Performance 350Z '04 
[ ] Opera Performance S2000 '04 [P]

== [ ] Pagani ==
[ ] Pagani Zonda C12 '00
[ ] Pagani Zonda C12S '00
[ ] Pagani Zonda C12S 7.3 '02
[ ] Pagani Zonda LM Race Car '01 [P]

== [ ] Panoz ==
[ ] Panoz Esperante GTR-1 Race Car '98

== [ ] Pescarolo ==
[ ] Pescarolo Courage C60/Peugeot Race Car '03
[ ] Pescarolo PlayStation Pescarolo C60/Judd Race Car '04

== [ ] Peugeot ==
[ ] Peugeot 106 Rallye '03
[ ] Peugeot 106 S16 '03
[ ] Peugeot 205 Turbo 16 '85
[ ] Peugeot 205 Turbo 16 Evolution 2 Rally Car '86 [P]
[ ] Peugeot 205 Turbo 16 Rally Car '85 [P]
[ ] Peugeot 206 Rally Car '99 (Exxon) 
[ ] Peugeot 206 RC '03
[ ] Peugeot 206 S16 '99
[ ] Peugeot 206cc '01
[ ] Peugeot 307 XSi '04
[ ] Peugeot 406 3.0 V6 Coupé '98
[ ] Peugeot 905 Race Car '92 (Exxon) 

== [ ] Plymouth ==
[ ] Plymouth Cuda 440 Six Pack '71
[ ] Plymouth Super Bird '70 [P]

== [ ] Polyphony Digital ==
[ ] Polyphony Digital Formula Gran Turismo '04 [P] [S]

== [ ] Pontiac ==
[ ] Pontiac GTO 5.7 Coupe '04
[ ] Pontiac Solstice Coupe Concept '02 [P]
[ ] Pontiac Sunfire GXP Concept '02 [P]
[ ] Pontiac Tempest Le Mans GTO '64
[ ] Pontiac Vibe GT '03

== [ ] Proto Motors ==
[ ] Proto Motors SPIRRA 4.6 V8 '04 

== [ ] RE Amemiya ==
[ ] RE Amemiya AMEMIYA ASPARADRINK RX7 (JGTC) '04

== [ ] Renault ==
[ ] Renault 5 Maxi Turbo Rally Car '85 [P]
[ ] Renault 5 Turbo '80 [P]
[ ] Renault AVANTIME '02 [P] [S]
[ ] Renault Clio Renault Sport 2.0 16V '02 
[ ] Renault Clio Renault Sport Trophy V6 24V Race Car '00 [P]
[ ] Renault Clio Renault Sport V6 24V '00 
[ ] Renault Clio Renault Sport V6 Phase 2 '03 
[ ] Renault Megane 2.0 16V '03
[ ] Renault Megane 2.0 IDE Coupe '00

== [ ] RUF ==
[ ] RUF 3400S '00
[ ] RUF BTR '86
[ ] RUF CTR "Yellow Bird" '87 [P]
[ ] RUF CTR2 '96
[ ] RUF RGT '00

== [ ] Saleen ==
[ ] Saleen S7 '02

== [ ] Scion ==
[ ] Scion xA '03
[ ] Scion xB '03

== [ ] Seat ==
[ ] Seat Ibiza Cupra '04

== [ ] Shelby ==
[ ] Shelby Cobra 427 '67
[ ] Shelby Mustang G.T. 350R '65 [P]
[ ] Shelby Series 1 Super Charged '03

== [ ] Spoon ==
[ ] Spoon CIVIC TYPE R (EK) '00
[ ] Spoon Fit Race Car '03
[ ] Spoon INTEGRA TYPE R (DC2) '99
[ ] Spoon S2000 '00
[ ] Spoon S2000 Race Car '00

== [ ] Spyker ==
[ ] Spyker C8 Laviolette '02

== [ ] Subaru ==
[ ] Subaru CUSCO SUBARU ADVAN IMPREZA (JGTC) '03
[ ] Subaru IMPREZA Coupe WRXtypeR STi Version VI (GC) '99
[ ] Subaru IMPREZA Premium Sport Coupe 22B-STi Version (GC) '98
[ ] Subaru IMPREZA Rally Car '01 [P]
[ ] Subaru IMPREZA Rally Car '03
[ ] Subaru IMPREZA Rally Car '99 [P]
[ ] Subaru IMPREZA Rally Car Prototype '01 [P]
[ ] Subaru IMPREZA Sedan WRX STi (GC) '94
[ ] Subaru IMPREZA Sedan WRX STi (GD, Type-II) '02
[ ] Subaru IMPREZA Sedan WRX STi spec C (GD, Type-II) '04
[ ] Subaru IMPREZA Sedan WRX STi Version (GD, Type-I) '00
[ ] Subaru IMPREZA Sedan WRX STi Version II (GC) '95
[ ] Subaru IMPREZA Sedan WRX STi Version III (GC) '96
[ ] Subaru IMPREZA Sedan WRX STi Version IV (GC) '97
[ ] Subaru IMPREZA Sedan WRX STi Version V (GC) '98
[ ] Subaru IMPREZA Sedan WRX STi Version VI (GC) '99
[ ] Subaru IMPREZA Sport Wagon STi (GG, Type-I) '00
[ ] Subaru IMPREZA Sport Wagon WRX STi Version VI (GF) '99
[ ] Subaru IMPREZA Super Touring Car '01 [P]
[ ] Subaru IMPREZA WRX STi Prodrive Style (GD, Type-I) '01
[ ] Subaru LEGACY B4 2.0GT '03
[ ] Subaru LEGACY B4 2.0GT specB '03
[ ] Subaru LEGACY B4 3.0R '03
[ ] Subaru LEGACY B4 Blitzen '00
[ ] Subaru LEGACY B4 RSK '98
[ ] Subaru LEGACY Touring Wagon 2.0GT '03
[ ] Subaru LEGACY Touring Wagon 2.0GT specB '03
[ ] Subaru LEGACY Touring Wagon 3.0R '03
[ ] Subaru LEGACY Touring Wagon GT-B '96
[ ] Subaru SUBARU 360 '58 [P] [S]

== [ ] Suzuki ==
[ ] Suzuki ALTO LAPIN Turbo '02
[ ] Suzuki ALTO WORKS RS-Z '97
[ ] Suzuki ALTO WORKS SUZUKI SPORT LIMITED '97
[ ] Suzuki Cappuccino (EA11R) '91
[ ] Suzuki Cappuccino (EA21R) '95
[ ] Suzuki CONCEPT-S2 '03 [P]
[ ] Suzuki ESCUDO Dirt Trial Car '98 [P]
[ ] Suzuki GSX-R/4 '01 [P]
[ ] Suzuki Kei WORKS '02
[ ] Suzuki MR Wagon Sport '04
[ ] Suzuki WAGON R RR '98

== [ ] Tom's ==
[ ] Tom's X540 CHASER '00

== [ ] Tommy kaira ==
[ ] Tommy kaira ZZ-S '00
[ ] Tommy kaira ZZII '00

== [ ] Toyota ==
[ ] Toyota 2000GT '67
[ ] Toyota 7 Race Car '70 [P]
[ ] Toyota ALTEZZA Touring Car '01 [P]
[ ] Toyota au CERUMO Supra (JGTC) '01
[ ] Toyota CALDINA GT-FOUR '02
[ ] Toyota CARINA ED 2.0 X 4WS '89
[ ] Toyota Castrol Tom's Supra (JGTC) '00
[ ] Toyota Castrol Tom's Supra (JGTC) '01
[ ] Toyota CELICA 1600GT (TA22) '70
[ ] Toyota CELICA 2000GT-FOUR (ST165) '86
[ ] Toyota CELICA 2000GT-R (ST162) '86
[ ] Toyota CELICA GT-FOUR (ST205) '98
[ ] Toyota CELICA GT-FOUR Rally Car (ST185) '95 [P]
[ ] Toyota CELICA GT-FOUR Rally Car (ST205) '95 [P]
[ ] Toyota CELICA GT-FOUR RC (ST185) '91
[ ] Toyota CELICA GT-R (ST183, 4WS) '91
[ ] Toyota CELICA SS-II (ST202) '97
[ ] Toyota CELICA SS-II (ZZT231) '99
[ ] Toyota CELICA XX 2800GT '81
[ ] Toyota COROLLA LEVIN BZ-R '98
[ ] Toyota COROLLA LEVIN GT-APEX (AE86) '83
[ ] Toyota COROLLA Rally Car '98
[ ] Toyota COROLLA RUNX Z AEROTOURER '02
[ ] Toyota DENSO SARD SUPRA GT (JGTC) '00
[ ] Toyota GT-ONE Race Car (TS020) '99 (Exxon) A black version of the car is available on the 100th used car cycle (days 694-700) in Late '90s Used Cars Showroom
[ ] Toyota MINOLTA Toyota 88C-V Race Car '89 [P]
[ ] Toyota Motor Triathlon Race Car '04 [P]
[ ] Toyota MR2 1600 G '86
[ ] Toyota MR2 1600 G-Limited Super Charger '86
[ ] Toyota MR2 G-Limited '97
[ ] Toyota MR2 GT-S '97
[ ] Toyota MR2 Spyder '99 
[ ] Toyota MR2 Spyder (6-speed sequential manual transmission) '02 
[ ] Toyota PRIUS G (J) '02
[ ] Toyota Prius G Touring Selection (J) '03
[ ] Toyota RSC '01 [P]
[ ] Toyota RSC Rally Raid Car '02 [P]
[ ] Toyota SERA '92
[ ] Toyota SPORTS 800 '65
[ ] Toyota SPRINTER TRUENO BZ-R '98
[ ] Toyota SPRINTER TRUENO GT-APEX (AE86) '83
[ ] Toyota SPRINTER TRUENO GT-APEX (AE86) Shuichi Shigeno Version '00
[ ] Toyota STARLET Glanza V '97
[ ] Toyota SUPERAUTOBACS APEX MR-S (JGTC) '00
[ ] Toyota SUPRA 2.5GT Twin Turbo R '90
[ ] Toyota SUPRA 3.0GT Turbo A '88
[ ] Toyota SUPRA RZ '97
[ ] Toyota SUPRA SZ-R '97
[ ] Toyota Tacoma X-Runner '04
[ ] Toyota VITZ F '99 
[ ] Toyota VITZ RS 1.5 '00 
[ ] Toyota VITZ RS Turbo '02 [P] [S]
[ ] Toyota VITZ U Euro Sport Edition '00 
[ ] Toyota VOLTZ S '02
[ ] Toyota WEDSSPORT CELICA (JGTC) '03
[ ] Toyota WiLL VS '01
[ ] Toyota WOODONE Tom's Supra (JGTC) '03

== [ ] TRD ==
[ ] TRD Toyota Modellista CELICA TRD Sports M (ZZT231) '00

== [ ] Trial ==
[ ] Trial CELICA SS-II (ZZT231) '03

== [ ] Triumph ==
[ ] Triumph Spitfire 1500 '74 [P] [S]

== [ ] TVR ==
[ ] TVR Cerbera Speed 12 '00 [P] [S]
[ ] TVR Cerbera Speed Six '97
[ ] TVR Griffith 500 '94
[ ] TVR T350C '03
[ ] TVR Tamora '02
[ ] TVR Tuscan Speed 6 '00
[ ] TVR V8S '91

== [ ] Volkswagen ==
[ ] Volkswagen Beetle 1100 Standard (Type-11) '49 [P]
[ ] Volkswagen Bora V6 4MOTION '01
[ ] Volkswagen Golf I GTI '76 [P]
[ ] Volkswagen Golf IV GTI '01
[ ] Volkswagen Golf IV R32 '03
[ ] Volkswagen Golf V GTI '05
[ ] Volkswagen Karmann Ghia Coupe (Type-1) '68 [P]
[ ] Volkswagen Lupo Cup Car '00
[ ] Volkswagen Lupo GTI '01
[ ] Volkswagen Lupo GTI Cup Car (J) '03
[ ] Volkswagen Lupo1.4 '02 [P]
[ ] Volkswagen New Beetle 2.0 '00
[ ] Volkswagen New Beetle Cup Car '00
[ ] Volkswagen New Beetle RSi '00
[ ] Volkswagen Polo GTI '01
[ ] Volkswagen W12 Nardo Concept '01 [P]

== [ ] Volvo ==
[ ] Volvo 240 GLT Estate '88 [P]
[ ] Volvo S60 T 5 Sport '03

